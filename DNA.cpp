/**
 * Copyright: Yao Yao
 */

#include "DNA.h"
using namespace std; 
DNA::DNA()
{
}
 
 
DNA::DNA(int ID,int  DfID,int DNA2ID,int DLength,int* dsequence,Rule* Ru) 
{
  shareNum=1;
 DNAID=ID;
  DNAfamilyID=DfID;
 DNALength   = DLength; 
DNAcID =DNA2ID;  
 RuleB=Ru;  
  
  Nsequence=new int[MaxDNALength];
for(int i=0;i<DNALength; i++) 
{
 Nsequence[i]=dsequence[i];
}  
vector<int> dat(3);
 dat[0] =0;  dat[1] =0;  dat[2]  =0;
   subtyp.push_back(dat);
} 


void DNA::Update (int ID,int  DfID,int DNA2ID,Rule* Ru,int * sequence,  int length)
{
  shareNum=1;
 DNAID=ID;
  DNAfamilyID=DfID;
 DNALength  =length; 
DNAcID =DNA2ID;  
 RuleB=Ru;
for(int i=0;i<DNALength; i++) 
{
 Nsequence[i]=sequence[i];
}  
vector<int> dat(3);
 dat[0] =0;  dat[1] =0;  dat[2]  =0;
   subtyp.push_back(dat);
}

 int DNA::SubCheck( int * g, int q)
  {
 if(q>=0)
 {   
   int tsequence[DNALength];
 
  for(int i=0;i<DNALength; i++) 
{
   tsequence[i]  = -5;
}


 int cq=q;

  while(cq!=0)
 {
if(tsequence[subtyp[cq][1]]<0)
 tsequence[subtyp[cq][1]]=subtyp[cq][2];
   cq = subtyp[cq][0];
 }
  for(int i=0;i<DNALength; i++) 
{
  if(tsequence[i]<0)
   tsequence[i]  = Nsequence[i];
}

 
    for(int i=0;i<DNALength; i++) 
{
 if (tsequence[i]!=g[i])
{
vector<int> dat(3);
 dat[0] =q;  dat[1] =i;  dat[2]  =g[i];
   subtyp.push_back(dat);
 return (subtyp.size()-1);
}
} 
}
     return 0;
  }


void DNA::DeleteDNA()
{  

   DNAfamilyID=-1; 
    DNALength=0;
	 DNAcID=-1;
	  shareNum =0;  
  //   cerr<<"  DNA "  << endl;
vector< vector<int> >(0).swap(subtyp);  
}


void DNA::PrintoutDNA(string &DNAseq)
{
       stringstream n;
          	
DNAseq="";	
for(int i=0;i<DNALength; i++) 
{

 n<<  Nsequence[i]; 	
 
}  
DNAseq  =DNAseq+n.str();	
}



