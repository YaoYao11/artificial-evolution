/**
 * Copyright: Yao Yao
 */
#include "Event.h"
 
using namespace std; 
Event::Event()
{
}
 
Event::Event (Robot ** R1,  Gene ** Ge0,Rule* Ru1,int de, int * condit,  int cum  , int eng, double decay)
{
 vector< vector<int> > conditions;
 Deter =de; 
 empty=false;
 for(int i=0;i<cum;i++)
 {
	 Con.push_back(condit[i]);
 }
 Rul=Ru1;
 Rlist=R1;
 Glist  = Ge0;
 top=0; 
 extop  = 0 ;
 dtop=0; 
 lifetime=0;
 credit=eng;
 decayRate  = decay;  
}

void Event::Renew (int de, int * condit,  int cum  , int eng, double decay)
{
Deter =de; 
empty=false;
for(int i=0;i<cum;i++)
{
 Con.push_back(condit[i]);
}
top=0; 
extop  = 0 ;
dtop=0; 
lifetime=0;
credit=eng;
decayRate  = decay;  
}


bool Event::Update   (double m)
{
	if(empty)
   return false;
    else
 {
if(Robots.size()>=1)
{
  vector< int > (0).swap(Robots); 
}
if(results.size()>=1)
{
  vector< bool > (0).swap(results); 
}
AvM = m;
dtop=top-extop;
extop=top;
credit=credit+dtop*3;
if(credit<=10)
{ 
DeleteEvent();
   return false;
}
else
{
credit=credit*(1-decayRate);
lifetime++;
if(lifetime>100)
credit=credit+(lifetime/100.0)*5;
credit=credit  + top;
   return true;
  }
 }
   }
 void Event::DeleteEvent  ()
{
  empty  = true;
credit  =0;  
 lifetime=0; 
top  = 0;
 decayRate   =0;
 vector< bool >(0).swap(results); 
vector< int >(0).swap(Robots);
vector< int >(0).swap(Con);
vector< vector<int> >(0).swap(conditions);
}


void Event::record   ( int c)
{
       stringstream ss;
       ss  << c;

   string file="result/Event___at_" +ss.str()  +".txt";
    ofstream out(file.c_str(),ios::app); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 			   
   
   
  out<<"  credit " <<credit<<"  Deter "<<  Deter<<"  top "<<top<<"lifetime"<<lifetime<<endl;
  
         out<<"  : " <<Con[0]<<"   "<<Con[1]<<" "<<Con[2]<<" "<< Con[3]<<endl;
  

  
  
for(int m=0;m<Robots.size();m++) 
{


out<<Robots[m]<<" ";

}

   out<<endl;
  for(int m=0;m<results.size();m++) 
{


out<<results[m]<<" ";

}
   out<<endl;
    out<<endl;   
  out.close();
}

inline bool Event::CheckMR(int x)
{
// if(Rlist[x]->Mucount>0)
  if(Rlist[x]->HMucount>0)
   return true;
else
  return false ;
}
  bool Event::CollectData  (int r1)
{
bool rel;  
 rel  = false;
bool relv; 
 relv=true;
 

   for(int i=1; i<Con.size()-1; i++) 
{


switch  (Con[i])
      {
  case 0:
{
break;
}         
case 1:
{  
 if(Rlist[r1]->sen[2]>=1.5)
 {
 }
 else
  return false;
break;
}
  case 2:
{
 if(Rlist[r1]->sen[2]<1)
 {

 }
 else
  return false; 
break;
}         
case 3:
{
 if(Rlist[r1]->sen[1]>3)
 {

 }
 else
  return false;
break;
}

  case 4:
{
if(Rlist[r1]->sen[1]<=1)
 {

 }
 else
  return false;
break;
}
case 5:
{
 if(Rlist[r1]->energy>20000)
 {

 }
 else
  return false;
break;
}

  case 6:
{
if(Rlist[r1]->energy<=12001)
 {

 }
 else
  return false;
break;
}


case 7:
{
 if(CheckMR(r1))
 {

 }
 else
  return false;
break;
}

  case 8:
{
if(!CheckMR(r1))
 {

 }
 else
  return false;
break;
}

case 9:
{
 if(Rlist[r1]->Duplicate>=1)
 {

 }
 else
  return false;
break;
}

  case 10:
{
if(Rlist[r1]->Duplicate<1)
 {
 
 
 
 }
 else
  return false;
break;
}

case 15:
{

     double  expressv=Rlist[r1]->GetExpressv();

	 
	 
if(expressv>=Rlist[r1]->expressh*0.89&&expressv<=Rlist[r1]->expressh*1.1)
{
     Rlist[r1]->expressh=expressv;
 }
 else
 {
   Rlist[r1]->expressh=expressv;
  return false;
  }

break;
}
		 
default:
		   break;
      }
		   
   }

 switch(Deter)
  {
  case 0:
{
break;
}         
case 1:
{  
 if(Rlist[r1]->sen[2]>=1.5)
 {
   relv  = true;
 }
 else
  relv  = false;
break;
}
  case 2:
{
 if(Rlist[r1]->sen[2]<1)
 {
  relv  = true; 
 }
 else
  relv  = false; 
break;
}         
case 3:
{
 if(Rlist[r1]->sen[1]>3)
 {
   relv  = true;
 }
 else
  relv  = false;
break;
}

  case 4:
{
if(Rlist[r1]->sen[1]<=1)
 {
   relv  = true;
 }
 else
  relv  = false;
break;
}
case 5:
{
 if(Rlist[r1]->energy>20000)
 {
 
   relv  = true;
 }
 else
  relv  = false;
break;
}

  case 6:
{
if(Rlist[r1]->energy<=12001)
 {
 relv  = true;  
 }
 else
  relv  = false;
break;
}
case 7:
{
 if(CheckMR(r1))
 {
   relv  = true;
 }
 else
  relv  = false;
break;
}

  case 8:
{
if(!CheckMR(r1))
 {
 relv  = true;
 }
 else
  relv  = false;
break;
}
case 9:
{
 if(Rlist[r1]->Duplicate>1)
 {
   relv  = true;
 }
 else
   relv  = false;
break;
}

  case 10:
{
if(Rlist[r1]->Duplicate==1)
 {
       relv  = true;
 }
 else
  relv  = false;
break;
}
  case 11:
{
if(Rlist[r1]->Murate>AvM)
 {
       relv  = true;
 }
 else
  relv  = false;
break;
}
	 
default:
		   break;
      } 
 
   
   
rel  = true; 
  
  Robots.push_back(r1);
   results.push_back  (relv);

 return rel;
}
