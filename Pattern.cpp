/**
 * Copyright: Yao Yao
 */
 
#include "Pattern.h"

using namespace std; 

Pattern::Pattern()
{
empty=true;
}

 
Pattern::Pattern  (int id,int eid1,int eid2,double cr,double a,double tra,double d,vector<Pattern*> * pl,vector<Event*> * el)
{
ID  = id+10000; EID1  = eid1; EID2  = eid2  ;
Plist=pl;Elist=el;
tend  =0;
htend =0;
aim  = a;  // not  = 0.5 to 1
 trange=tra;
 decayRate=d;  credit  = cr;
empty = false;
top=0;
 output=false;
  lifetime =0;
   hpos=0;
  hneg=0;
   cerr<<"  ok "<<endl;
updated  = false; 
 
}

  void Pattern::Renew (int id,int eid1,int eid2,double cr,double a,double tra,double d)
{
ID  = id+10000; EID1  = eid1; EID2  = eid2  ;
tend  =0;
htend =0;
aim  = a;  // not  = 0.5 to 1
 trange=tra;
 decayRate=d;  credit  = cr;
empty = false;
top=0;
 output=false;
  lifetime =0;
   hpos=0;
  hneg=0;
   cerr<<"  ok  pattern renen "<<endl;
updated  = false; 
}

int Pattern::Decide()
{

int n1=(*Elist)[EID1]->results.size();
 int n2=(*Elist)[EID2]->results.size();
if(n1>0 && n2>0)
{
pos =0;
neg =0;
for(int i=0;i<n1;i++)
{
for(int z=0;z<n2;z++)
{
if((*Elist)[EID1]->results[i]==(*Elist)[EID2]->results[z])
pos++;
else
neg++;
}
}
if((pos+neg)!=0)
tend=pos/(pos+neg);
  hpos=hpos+pos;
  hneg=hneg+neg;
   if((hpos+hneg)!=0)
 htend=hpos/(hpos+hneg);
}
else
return 0;
return 7; 
}

void  Pattern::Update()
{
double compare;
  compare=0;

if(aim>0.5)
{

  if(aim>tend)
  {
  compare  = (aim-tend)/trange; 
   credit=credit-compare;
      output=false;

	  
  }
  if(aim>htend)
  {
compare  = (aim-htend)/trange; 
   credit=credit-(compare*1.5);	  
  }  
    if(aim<tend)
  {
  compare  = (tend-aim)/trange; 
   credit=credit + compare;
  output=true;
   }
      if(aim<htend)
  {
  compare  = (htend-aim)/trange; 
   credit=credit + (compare*1.5);
   }
}
if(aim< 0.5)
{
  if(aim>tend)
  {
  compare  = (aim-tend)/trange; 
   credit=credit+compare;
  output=true;
  }

  
   if(aim>htend)
  {
  compare  = (aim-htend)/trange; 
 credit=credit + (compare*1.5);
  } 
  
    if(aim<tend)
  {
  compare  = (tend-aim)/trange; 
   credit=credit - compare;
     output=false;
   }
 if(aim<htend)
  {
  compare  = (htend-aim)/trange; 
 credit=credit-(compare*1.5);
 
   }
}

if(credit<=10)
{ 
this->deletePattern();
}
else
{
credit=credit*(1-decayRate);
lifetime++;
if(lifetime>100)
credit=credit+(lifetime/100.0)*5;
credit=credit  + top*0.5;
updated=true;
}
}

void  Pattern::deletePattern()
{
empty  = true;
credit  =0;
 hpos=0;
  hneg=0;
 lifetime=0;
 pos=0; 
  neg=0;
   tend =-1 ;
  aim=-1;  // not  = 50 or 100
 trange=1;   //  can not be 0
  decayRate   =0;
}
  void Pattern::record   ( int c)
{
       stringstream ss;
       ss  << c;

   string file="result/Pattern___at_" +ss.str()  +".txt";
    ofstream out(file.c_str(),ios::app); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 			   
   
   
  out<<"  credit " <<credit<<" positive count "<<pos<<"  top "<<top<<"  lifetime "<<lifetime<<" tend " <<  tend<<"  htend "<<  htend<<endl;
out<<" pos " <<pos<<"  hpos "<<hpos<<" neg "<<neg<<"  hneg "<<hneg<<endl;  
   out<<"  ID " <<ID-10000<<" EID "<<EID1<<" EID "<<EID2<<" range "<<trange<<endl;
  out<<endl; 
out<<endl; 
  out.close();
}
       int  Pattern::CheckDevRec()
	    {        
   if(credit<100&&lifetime>37)
  return 2; 
 if(credit<100&&lifetime>28)
  return 1;
    if(credit<100&&lifetime>18)
  return 4;
   if(credit<100&&lifetime>10)
  return 5;
 if(credit<28)
  return 6;
   if(credit>710&&lifetime>20)
  return 3;
   return 0;
			   }
int  Pattern::Develop(int typ)
{
if(typ==0)
return -1;
int rec;
  rec  = -1;
 int  createdPat  = (*Plist).size();
  int createdEv  = (*Elist).size();
double ncredit;   
double naim;  // not  = 0.5 to 1
double ntrange;
double ndecayRate;  
 

 ncredit  = credit;
  naim = aim;
 ntrange  = trange;
  ndecayRate=decayRate;
  
  
     if(typ>3)

	 {
  srand((unsigned)time(NULL)+rand());
     for(int i=0;i< (typ-3);i++)
  {
  switch  (rand()%4)
      {
  case 0:
{
ncredit=rand()%270+50;
break;
}         
case 1:
{
naim=(rand()%99+1)/100;

if(naim==0.5)
{
if(rand()%2 ==1)
naim=0.55;
else
naim  =   0.45;
}
break;
}
  case 2:
{
  ntrange  =   (rand()%15+1)/100; 
break;
}         
case 3:
{
ndecayRate  = (rand()%46+5)/100;
break;
}			 
default:
		   break;
      }
  
} 


  
  }
     if(typ!=3)
	 {
  bool cre =true;
 for(int i=0;i<createdPat;i++)
 {
 if((*Plist)[i]->empty)
 {
  rec  = i; i  = createdPat+9;
   cre  =false;
    }
 }
 
   if(cre&&createdPat<10000)
   rec=createdPat;
 if(rec>=0&&(createdPat>=5||createdEv>=5))
  { 
  
	 int eid1; int eid2; 
	  bool c1  = true;
	   int c3  = 1;
	
while(c1)
{		
   if(typ==1||typ==6)
  {
     if(rand()%2==0)
eid1  = rand()%createdPat+10000;   
else
  eid1  = rand()%createdEv; 
if(rand()%2==0)
eid2= rand()%createdPat+10000;   
else
  eid2 = rand()%createdEv; 
  }
     if(typ==5||typ==2)
  {
  if(rand()%2==0)
  {
  if(rand()%2==0)
  eid2=rand()%createdPat+10000;
  else
  eid1  = rand()%createdPat+10000;   
  }
  else
  {
    if(rand()%2==0)
  eid2=rand()%createdEv;
  else
  eid1  = rand()%createdEv; 
  }
 }
     if(typ==4)
  { 
eid1=EID1;  
 eid2=EID2; 
  c1  =false;
  }
   if(c1)
   {
   int e1=0;
    int  e2 =0;
	
	c1  = false;
	
   if(eid1>=10000)
   {
   e1=eid1-10000; 
   if((*Plist)[e1]->empty)
   c1  =true;
   }
   else
   {
   e1  = eid1;
     if((*Elist)[e1]->empty)
   c1  =true; 
   }
   if(eid2>=10000)
   {
   e2=eid2-10000;
     if((*Plist)[e2]->empty)
   c1  =true; 
   }
   else
   {
   e2  = eid2;
  if((*Elist)[e2]->empty)
   c1  =true; 
 }
   if( e1==e2)
            c1=true;   
     }
 c3++;
  if(c3>10&&c1)
  {
 c1=false;
   c3  = 99;
 }
  }
   if(c3!=99)
 {
  
 if(cre)
   {
 Pattern *  npa  = new  Pattern(rec,eid1,eid2,credit,aim, trange,decayRate, Plist, Elist);  
  (*Plist).push_back(npa);
}
else
{
  (*Plist)[rec]->Renew(rec,eid1,eid2,credit,aim, trange,decayRate);   
} 
  }

   }
   }
  else
  {
    credit  = credit +100;
  rec  = ID-10000;
  }
  return rec;
}

/*

   int * tp = new int[patSize];  
 for(   int z=0; z<patSize;z++)
{

}
Co.push_back(tp);
int * tp2 = new int[patSize];  
 for(   int z=0; z<patSize;z++)
{
}
         PCo.push_back(tp2);
   
      
	  */