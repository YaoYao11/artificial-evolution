/**
 * Copyright: Yao Yao
 */
#include "Mutation.h"
using namespace std; 
Mutation::Mutation()
{
}

Mutation::Mutation(Gene **gene,DNA ** dna,Robot ** robots,Rule * Ragent)
{
targetDNA=dna;
targetGene=gene; 
  Ra  = Ragent;
robotlist=robots;
 
RID=-1;
updateGene= new int[1000];
updateDNA =new int [6000];
genes = new int[9000];
newGID =0;
newID=0; 

  createdGeneNum =0;
 createdDNANum =0;
for(int i=0;i<9000;i++)
{
genes[i]=-1;
}

}


void  Mutation::LoadGseq  (int F_ID,int F_ID2,int B_ID, int typ , int st)   

{
   type =typ; 
 startpoint=st;
 if(type==1)
   Le=targetDNA[F_ID]->DNALength+targetDNA[F_ID2]->DNALength;
if(type==2)
  Le=targetDNA[F_ID]->DNALength;
   
if(type==3)
Le=targetGene[F_ID]->GeneLength;
if(type==4)
Le=targetGene[F_ID]->GeneLength+targetDNA[B_ID]->DNALength;

if(type>=3)
{
Mutle =targetGene[F_ID]->GeneLength;
for(int i=0; i < Mutle;i++)
 { genes [i] =  targetGene[F_ID]->  Gsequence[i];
 }
   int distance=Le - Mutle;
   
if(type==4)
{   
 for(int i=0; i  < distance;i++)
{
genes [i+Mutle] =  targetDNA[B_ID]->   Nsequence[i];     
 }
}
}
else
{
Mutle  = targetDNA[F_ID]->DNALength;
for(int i=0; i < Mutle;i++)
 { genes [i] =  targetDNA[F_ID]->   Nsequence[i];
 }
   int distance=Le - Mutle;

  if(type==1)
  {
 for(int i=0; i  < distance;i++)
{

genes [i+Mutle] =  targetDNA[F_ID2]->   Nsequence[i];
      // if(type==2)
  // genes [i+Mutle] =  targetGene[DNA_ID]->  Gsequence[i];  
 }
}
}


 //cerr<<" Mutle "<<Mutle<<endl;
 }

 
  int Mutation::replaceDNA(  int* seq  ,int length,int  ID,int DID  , int FID, bool m)
  {
    int retid; 
	int lastid;
	   retid=this->replaceDNA(seq  ,length,ID, DID,FID );  
    lastid=Ra->mcount-1;
     Ra->MutationRecord[lastid][5]  = retid; 
   Ra->MutationRecord[lastid][4]=-1;
   return  retid;	 
  }
 int Mutation::replaceDNA(  int* seq  ,int length,int  ID,int DID  , int FID)

 {//cerr<<" replace " <<endl;
 bool ext;
  ext=true;
 if(ID>=0)
 {
   targetDNA [ID]->shareNum--;  
    }
	
   if(  ID<0||targetDNA [ID]->shareNum<=0)
   {
   for(int i=0;i< createdDNANum;i++)
   {
   if(targetDNA [i]->shareNum<=0)
   {
  ext=false;
   ID=i;
   i=createdDNANum+1;
   }
   }
 
   }

   
  if (ext)
  {
      targetDNA[createdDNANum]=new  DNA(createdDNANum,  FID, DID,length, seq,Ra);
	ID=createdDNANum;   createdDNANum++;	  
  }
  else
   {
     targetDNA [ID]->DNAfamilyID   =FID;
   targetDNA [ID]->DNAcID=DID;
    targetDNA [ID]->shareNum=1;
  targetDNA [ID]->DNALength  = length;
   for(int i=0; i < length;i++)
 { 
   targetDNA [ID]->Nsequence[i] =seq[i];
 }

 }
  return  ID;
}


    int Mutation::createGene(int* seq, int length,int  ID,int GID  , int FID,bool m)
  {
    int retid; 
	int lastid;
	   retid=this->createGene( seq,  length,ID,GID  ,FID);  
	    lastid=Ra->mcount-1;
     Ra->MutationRecord[lastid][5]  = retid;  
  

   Ra->MutationRecord[lastid][4]=-7;	 
   return  retid;	 
  } 
  
 int Mutation::createGene(int* seq, int length,int  ID,int GID  , int FID)
 {
 
 
 
   bool ext;
  ext=true;
 if(ID>=0)
 {
   targetGene  [ID]->shareNum--;  
    } 
	
	
    if(  ID<0||targetGene   [ID]->shareNum<=0)
	{
for(int i=0; i <createdGeneNum;i++)
 {
 if(targetGene [i]->shareNum<=0)
 {
 ext=false;
  ID=i;  
 i  = createdGeneNum+1;
  }   
 }
 }
 
  if (ext)
  {
     targetGene[createdGeneNum]=new  Gene(createdGeneNum,  FID,GID,seq, Ra,length   );
	ID=createdGeneNum;   createdGeneNum++;	  
  }
  else
   {
 targetGene [ID]->GenefamilyID   =FID;
   targetGene[ID]->GenecID= GID;
    targetGene [ID]->shareNum=1;
  targetGene [ID]->GeneLength  = length;


 
  for(int i=0; i < length;i++)
 { 
   targetGene [ID]->Gsequence[i] =seq[i];
 }
 
   
 
      targetGene[ID]->CMu(); 
  cerr <<" reuse gene  " <<ID << endl;
   }
   return   ID;
 }
   /*
 int Mutation::createDNA   (int* sequence, int length)
 {
 int i;
 i=0;
 int rec;
 rec=0;
 while(i<Dnumber)
 {
 if(DNAdata[i][0]==0)
 {
  rec=i;
  DNAdata[i][0]=1;
 targetDNA[i]->Update(i,i,i,Ra,sequence,length);
  return rec;
  }   i++;
 }
return rec;
 }
*/
 
void Mutation::deleteGene(int address)
 {
 
 if(  address>=0)
 {
   targetGene  [address]->shareNum--;
 }
 if(targetGene[address]->shareNum<=0)
 {
   targetGene[address]->DeleteG();
 } 
 }
 
 
 void Mutation::deleteDNA(int address)
 {
 if(  address>=0)
 {
   targetDNA[address]->shareNum--;  
    } 
 if(targetDNA[address]->shareNum<=0)
 {
   targetDNA[address]->DeleteDNA();
 }
 }


	int Mutation::MergeDNA  (vector< vector<int> > * D)
	{
	int t; t=0;	int M_num;
	M_num=0;
	for(int h=0; h<(*D).size();h++)
	{
	if((*D)[h][2]==0)
	{
	t = targetDNA[(*D)[h][0]]->DNALength+targetDNA[(*D)[h+1][0]]->DNALength;
	if(t  <= 5000)
	{
	for(int i=0; i <targetDNA[(*D)[h][0]]->DNALength;i++)
	{ 
	genes[i] =  targetDNA[(*D)[h][0]]->   Nsequence[i];
	}
	for(int i=0; i <  targetDNA[(*D)[h+1][0]]->DNALength ;i++)
	{ 
      genes[i+targetDNA[(*D)[h][0]]->DNALength] =  targetDNA[(*D)[h+1][0]]->  Nsequence[i];
	}
	
 replaceDNA( genes,t,(*D)[h+1][0],newID, newFID);  newID++;  newFID++; 
	deleteDNA((*D)[h][0]);
	
 (*D).erase((*D).begin()+h);  
 
  M_num++;
  
  
	}
	
	}
	
	}	
	return M_num;
	}
	
	int Mutation::DepartDNA  (vector< vector<int> > * D)
  {
	int t; t=0;	int M_num;
	M_num=0;
	int z;
	z=0;
	for(int h=0; h<(*D).size()  ; h++)
	{
	if(targetDNA[(*D)[h][0]]->DNALength>5300)
	{ 
   
        t=  targetDNA[(*D)[h][0]]->DNALength;	
	
int Dle= t%2;t  =t/2;
  for(int i=0;i<t;i++)
 {
   genes[i]  =targetDNA[(*D)[h][0]]->  Nsequence[i];
 }   
vector<int> dat(4);
(*D).insert((*D).begin()+h,  dat);

  (*D)[h][0] =replaceDNA(genes,  t, -1,newID,  newFID);  
   newID++; newFID++;
(*D)[h][1] =(*D)[ h+1][1];
(*D)[h][2]=0;
(*D)[h][3] =0;



  t  = t+Dle;
 for(int i=0; i <t;i++)
 {
  genes[i]=  targetDNA[(*D)[h+1][0]]->  Nsequence[i+targetDNA[(*D)[h][0]]->DNALength];
 } 

   
(*D)[h+1][0] =replaceDNA( genes,  t, -1,newID,  newFID);  
   newID++; newFID++;
(*D)[h+1][1] =(*D)[ h][1] +targetDNA[(*D)[h][0]]->DNALength;



(*D)[h+1][3] =0;
         M_num++;
   
   }
   }
  return M_num; 
	}

int Mutation::Havemutation  (vector< vector<int> > * G,vector< vector<int> > * D, vector< vector< vector<int> > > * C,vector< vector<double> > * A, bool &r1, int RobotID) 
{


RID=RobotID;

int h;
h=0;
int l;
l=0;
srand((unsigned)time(NULL)+rand());
//cerr<<" m  0 "  <<endl;
if(r1)
{




while((l+1)<(*G).size()||(h+1)<   (*D).size())
{
while((h+1)<   (*D).size()&&(*D)[h][2]==0)
{

 LoadGseq  ((*D)[h][0],(*D)[h+1][0],(*D)[h+1][0], 1,(*D)[h][1]);
  CheckNewGene(G,D,(*D)[h][0], (*D)[h+1][0], l,  h,   l-1, r1,true);
 h++;  
  
   //cerr<<" pass1 "  <<endl;
  
}
if((h+1)<(*D).size()&&(*D)[h][2]==1)
  {
 
  LoadGseq  ((*D)[h][0],(*D)[h+1][0],(*G)[l][0], 2,(*D)[h][1]); 
  CheckNewGene(G,D,(*D)[h][0], (*G)[l][0], l,  h,   l-1, r1,true);//cerr<<"  pass2 "  <<endl;
h++; 
   }
while((l+1)<(*G).size()  &&(*G)[l][2]==0&&(h+1)<(*D).size())
{



 LoadGseq  ((*G)[l][0], (*G)[l+1][0],(*G)[l+1][0],3,(*G)[l][1]);
  CheckMut (G  , D,(*G)[l][0], h,l, h,r1);
  l++;
}
if((l+1)<(*G).size()&&(*G)[l][2]==1&&(h+1)<(*D).size())
{ //

LoadGseq  ((*G)[l][0],(*G)[l+1][0],(*D)[h][0],4,(*G)[l][1]);

CheckMut (G  , D,(*G)[l][0], h,l,h,r1); 
  
   //cerr<<" m  35 "  <<" F-ID "<<(*G)[l][0]<<" ID "<< (*D)[h][0]<<endl;
l++;
}

if((l+1)<(*G).size()&&(h+1)==(*D).size())
{



//cerr<<" need to fulfil real function "  <<endl;
l++;
}
if((l+1)==(*G).size()&&(h+1)<(*D).size())
{
  //cerr<<" need to fulfil real function "  <<endl; 
  h++;
}

}
}
else
{ 
while((l+1)<(*G).size()||(h+1)<  (*D).size())
{
while((l+1)<(*G).size()&&(*G)[l][2]==0&&(h+1)<(*D).size())
{

LoadGseq  ((*G)[l][0],(*G)[l+1][0],(*G)[l+1][0],3,(*G)[l][1]);
CheckMut (G  , D,(*G)[l][0], h,l, h,r1);
  l++;
}
if((l+1)<(*G).size()&&(*G)[l][2]==1&&(h+1)<(*D).size())
{
LoadGseq  ((*G)[l][0],(*G)[l+1][0],(*D)[h][0],4,(*G)[l][1]);
CheckMut (G  , D,(*G)[l][0], h,l,h,r1) ; 
l++;
}
while((h+1)<   (*D).size()  && (*D)[h][2]==0)
{
LoadGseq  ((*D)[h][0], (*D)[h+1][0],(*D)[h+1][0], 1,(*D)[h][1]);
  CheckNewGene(G,D,(*D)[h][0], (*D)[h+1][0], l,  h,   l-1, r1,true);
 h++;
  
}
if((h+1)<   (*D).size() &&(*D)[h][2]==1)
{



LoadGseq  ((*D)[h][0],   (*D)[h+1][0],(*G)[l][0], 2,(*D)[h][1]);
  CheckNewGene(G,D,(*D)[h][0], (*G)[l][0], l,  h,   l-1, r1,true);
  h++;
 }
  if((l+1)<(*G).size()&&(h+1)==(*D).size())
{
//cerr<<" need to fulfil real function her "  <<endl;
l++;
}
if((l+1)==(*G).size()&&(h+1)<(*D).size())
{
//cerr<<" need to fulfil real function "  <<endl;
 h++;
}
}
}



//cerr<<"  pass9.5 "  <<endl;

  if(robotlist[RID]->GenomeLength<(*G).size())
{

//cerr<<"  pass9.7 "  <<endl;
 for( int h=robotlist[RID]->GenomeLength; h <(*G).size(); h++)           //
{  
 
  vector< vector<int> > dat(0);
 vector<int> dat3(2);
 //cerr<<"  h1 "<<  h<<endl; 
    vector<double> datdou(20); 
  for(   int i=0;  i <50; i++)
{
   vector<int> dat2(10);   
   
   
  for(   int z=0; z<10; z++)
{
   dat2[z]=0;
}

dat.push_back(dat2);
}

  (*C).push_back(dat); 
  (*A).push_back(datdou);
 robotlist[RID]->genehis.push_back(dat3);
   robotlist[RID]->vote.push_back(0);
  robotlist[RID]->vote1.push_back(0);
   }
 
 }
else
{
if(robotlist[RID]->GenomeLength>(*G).size())
{

 for( int h=(*G).size(); h <robotlist[RID]->GenomeLength; h++)           //
{  
//cerr<<"  h " <<h<< "  genome length "<< robotlist[RID]->GenomeLength<<endl;
if(((*C).begin()+h)<((*C).end()))
  (*C).erase((*C).begin()+h); 
if(((*A).begin()+h)<((*A).end()))
  (*A).erase((*A).begin()+h);
if((robotlist[RID]->genehis.begin()+h)<(robotlist[RID]->genehis.end()))   
 robotlist[RID]->genehis.erase(robotlist[RID]->genehis.begin()+h);


if((robotlist[RID]->vote.begin()+h)<(robotlist[RID]->vote.end()))    
robotlist[RID]->vote.erase(robotlist[RID]->vote.begin()+h);
if((robotlist[RID]->vote1.begin()+h)<(robotlist[RID]->vote1.end())) 
  robotlist[RID]->vote1.erase(robotlist[RID]->vote1.begin()+h); 
   }
 
 }
}
//cerr<<"  pass9 "  <<endl;


 return (*G).size();
   

}	



 
  void  Mutation::Mutate(bool &c1, int &pos,int t)
 {
 

 
  int th2;
mutation= mutation/100.0;



c1=false;
   th2 = rand()%10000;
mutation  = mutation*Mutle; 

//cerr<<"  mutle " << Mutle<<" th "<<th2 <<"  mutation "  <<  mutation<<endl;
         if( mutation  > th2)
	 //if (false)
{
  int mutpoint=0;   
  pos  = rand()%Mutle;
   mutpoint = rand()%4;  
   if(genes[pos]  == mutpoint)
   c1=false;
   else
   {   
   genes[pos]=mutpoint;    
   c1  = true;
   }
   
   
   
 if(c1&&RID>=0&&Ra->mcount>=Ra->MutationRecord.size())
 {
   //cerr<<" RID  " <<RID<<" mutation num "<<Ra->mcount <<" size " << Ra->MutationRecord.size()<<endl;
vector<int> dat(6);
 dat [0]  = robotlist[RID]-> xc;dat [1]  = robotlist[RID]->yc;  dat [2]  = RID; dat [3]  = pos;  dat [4]  =  mutpoint;  dat [5]  =  -t;
      Ra->MutationRecord.push_back(dat); 
   robotlist[RID]->Mucount++;
 robotlist[RID]->CMucount++;
  robotlist[RID]->HMucount++;
     Ra->mcount++;  
 }
 if(c1&&RID>=0&&Ra->mcount<Ra->MutationRecord.size())
 {  
  //cerr<<" not creat ne RID  " <<RID<<" mutation num "<<Ra->mcount <<" size " << Ra->MutationRecord.size()<<endl;  
   int index;
    index  = Ra->mcount;
  Ra->MutationRecord[  index][0]  = robotlist[RID]-> xc; Ra->MutationRecord[  index][1]  = robotlist[RID]->yc;  Ra->MutationRecord[  index][2]  = RID; Ra->MutationRecord[  index][3]  = pos; Ra->MutationRecord[  index][4]  =  mutpoint; 
         Ra->MutationRecord[  index][5]  = -t;
   Ra->mcount++; 
 robotlist[RID]->Mucount++;
  robotlist[RID]-> CMucount++; robotlist[RID]->HMucount++;  
 }
  }
  //cerr<<" ok with mutatc  " <<endl;
  
 }






	int Mutation::CheckMut  (  vector< vector<int> > * G  , vector< vector<int> > * D,int Fr, int location2,int location,int locationAfD  , bool &readD   )     // if location Afd none is        -1
{

    
int gene_Num; gene_Num = 1 ;
bool change;   
bool po;
   po=targetGene[Fr]->Protein;
 change  = false;
srand((unsigned)time(NULL)+rand());
mutation  = targetGene[Fr]->mutation;
int pos = 0;


 //cerr<<" checkmut " <<endl;
Mutate  (change,pos,1); 

/*
mutation= mutation/1000000;
   th2 = rand()%10000;
mutation  = mutation*Mutle;

if( mutation  <th2)
{
  int mutpoint  = 0;
  pos  = rand()%Mutle;
   mutpoint = rand()%4;
   if(genes[pos]  == mutpoint)
   change=false;
   else
   {   
   genes[pos]=mupoint;    
   change  = true;
   }

  }
  

*/
if(change)
{
if(pos  <=3)
{

if(location==0&&!readD)
	  readD=true;  
 else
 {
 if(location>=1  && (*G)[location-1][2]==0)
  (*G)[location-1][2]=1  ;
  else
  {
  
     
   
   if(location2>=1)
    (*D)[location2-1][2]   =0;
  }
 }
 
deleteGene((*G)[location][0]);
gene_Num--;
  if(Le<=5000)
  {  
   if(type==4)
   {
    (*D)[location2][0]  =replaceDNA(genes,Le,(*D)[location2][0] ,newID,targetDNA [(*D)[location2][0]]->DNAfamilyID,true)  ;
	  newID++;
	}
	if(type==3)
 {
   vector<int> dat2(4);
(*D).insert((*D).begin()+locationAfD,dat2);
    (*D)[locationAfD][0]  =replaceDNA(genes,Le, -1,newID,newFID,  true); newID++; newFID++;
     (*D)[locationAfD][1]  =(*G)[location][1];
	 (*D)[locationAfD][2]  =1;
	 (*D)[locationAfD][3]  =0;
}
(*G).erase((*G).begin() + location)  ;
   if(location>0&&(*G)[location-1][2]==0)
   (*G)[location-1][2]=1;
        // if(location>0&&(*G)[location-1][2]==1)  still =1
     
  }
 else
 {   
 
 int rest=Le%2;
  Le  =  Le/2;
  
 if(type==3)
 {
vector<int> dat7(4);
(*D).insert((*D).begin()+locationAfD,dat7);
vector<int> dat8(4);
(*D).insert((*D).begin()+locationAfD,dat8);
  for(int i=0; i <Le;i++)
 {
   updateDNA [i]=genes[i];
 }
    (*D)[locationAfD][0]  =replaceDNA(updateDNA,Le,-1,newID  , newFID,true); 
  newID++; newFID++;
     (*D)[locationAfD][1]  =startpoint;  (*D)[locationAfD+1][1]  =Le+startpoint;
	 (*D)[locationAfD][2]  =0;
	 (*D)[locationAfD][3]  =0;
   Le =Le+rest;
 for(int i=0;i<Le;i++)
 {
   updateDNA [i]=genes[ i + Le-rest];
 } 
   (*D)[locationAfD+1][0]  =replaceDNA(updateDNA,Le,  -1, newID  , newFID);  
  newID++; newFID++;
     
	 (*D)[locationAfD+1][2]  =0;
	 (*D)[locationAfD+1][3]  =0;
 }
 
 
 if(type==4)
{
vector<int> dat(4);
(*D).insert((*D).begin()+location2,  dat);
 

  for(int i=0; i <Le;i++)
 {
   updateDNA [i]=genes[i];
 }
 (*D)[location2][0] =replaceDNA(updateDNA,Le, -1,newID,  newFID,  true);   
  newID++; newFID++;
   (*D)[location2][1]=startpoint;
 (*D)[location2+1][1] =startpoint+Le;
 
   Le =Le+rest;
 for(int i=0;i<Le;i++)
 {
   updateDNA [i]=genes[ i + Le-rest];
 }  
 (*D)[location2  + 1][0] =replaceDNA(updateDNA,Le,(*D)[location2  + 1][0]  ,newID  , targetDNA [(*D)[location2  + 1][0]]->DNAfamilyID);
   newID++;
     //(*D)[location2+1][2] =(*D)[location2   ][2]; 
(*D)[location2 ][2] =0;
(*D)[location2+1][3] =0;  (*D)[location2][3] =0;
}
  }
  
 return  gene_Num;
}
else
{

int Mutle2=0;
Mutle2  = targetGene[Fr]->Mut(   pos, genes[pos]);   //default region is   fixed ,even equal 0 the number just equal to default value

if (Mutle2<=Le)
{
  deleteGene((*G)[location][0]);

for(int i=0; i <Mutle2 ;i++)
 {
   updateGene [i]=genes[i];
 }
  
//cerr<<"  32800 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl;  
  if ( Mutle != Mutle2&&Mutle2<=Le)
 {
targetGene  [(*G)[location][0]]->shareNum++;
 
  (*G)[location][0]=createGene(updateGene, Mutle2,(*G)[location][0]  , newGID  , targetGene [(*G)[location][0]]->GenefamilyID, true);   newGID++  ;
 //cerr<<"  32801 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl;  
int Dle=Le-Mutle2;       int rest=Dle%2;
      
if (Dle>0)
{

  if(  Dle<=5000 )
 {
   //cerr<<"  <= "<<endl; 
  for(int i=0; i <Dle;i++)
 {
   updateDNA [i]=genes[ i + Mutle2];
 }
 //cerr<<"  <= "<<endl; 
 }
  else
  { //cerr<<"  else "<<endl; 
  
  Dle  = Dle/2;
  for(int i=0; i <Dle;i++)
 {//cerr<<"  i "<<i<<" i+Mutle2 "<< i + Mutle2<<endl;
   updateDNA [i]=genes[ i + Mutle2];
   }
      //cerr<<"  else "<<endl;
   }
      //cerr<<"  32802 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl;  
  
  if(type==3)
{
vector<int> dat3(4);  
 (*D).insert((*D).begin()+ locationAfD,  dat3);
   (*D)[locationAfD][0]  =replaceDNA(updateDNA, Dle  , -1,newID,  newFID); 
   newID++; newFID++;
     (*D)[locationAfD][1]=startpoint +Mutle2;
	 (*D)[locationAfD][2]  =1;
	 (*D)[locationAfD][3]  =0;
}
 //cerr<<"  32803 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl; 
 if(type==4)
{
 (*D)[location2][0] =replaceDNA(updateDNA,Dle, (*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); 
  newID++;
  (*D)[location2][1] =startpoint +Mutle2; (*D)[location2][3]  =0;
}
   //cerr<<"  32805 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl; 
  if(Dle>5000) 
{
Mutle2  =Mutle2+Dle;
   Dle =Dle+rest;
   
 
 for(int i=0; i <Dle;i++)
 {
   updateDNA [i]=genes[ i + Mutle2];
 } /*
  if(type==3)
{
vector<int> dat5(4);  
 (*D).insert((*D).begin()+ locationAfD+1,  dat5);
   (*D)[locationAfD+1][0]  =replaceDNA( updateDNA,Dle );
     (*D)[locationAfD+1][1]=startpoint +Mutle2;	
  (*D)[locationAfD][2]=0;
	 (*D)[locationAfD+1][2]  =1;
	 (*D)[locationAfD+1][3]  =0;
}   


*/

//if(type==4){
 
  //cerr<<"  32806 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl; 
   vector<int> dat6(4);
(*D).insert((*D).begin()+location2+1,dat6);
 (*D)[location2+1][0] =replaceDNA(updateDNA,Dle, -1,newID,  newFID);  
   newID++; newFID++;
  //cerr<<"  32807 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl;    
(*D)[location2+1][1] =startpoint + Mutle2;
(*D)[location2+1][2] =(*D)[location2   ][2]; (*D)[location2 ][2] =0;
(*D)[location2+1][3] =0;  (*D)[location2][3] =0;





 //}
}
  
}
else
{
//cerr<<"  32808 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl;
if(type==4)
{
if((*D)[location2 ][2]==0)
(*G)[location ][2]  =1;
else
(*G)[location ][2]=0;
(*D).erase((*D).begin() + location2);
}
}



   //cerr<<"  328010 "<<createdDNANum<<" created gene "<<createdGeneNum<<endl;
}
if ( Mutle==Mutle2)
(*G)[location][0]=createGene(updateGene,Mutle2,(*G)[location][0]  , newGID  , targetGene [(*G)[location][0]]->GenefamilyID ,true);   newGID++; 
return  gene_Num;   
}
else
{Ra->deleteMRe();
}
}
}



return  gene_Num;
}

bool Mutation::CheckNewGene  (   vector< vector<int> > * G,vector< vector<int> > * D,int DNA_ID,int DNA_ID2  ,int  location, int location2 , int locationFG,  bool &readD, bool me) 
{

int gene_seq  = 0;
 if(type==1)
   gene_seq =targetDNA[DNA_ID2]->DNALength;
   //if(type==2)
        //gene_seq =targetGene[DNA_ID2]->GeneLength;
  //cerr<<"  cxl " <<endl;
   Mutle  = targetDNA[DNA_ID]->DNALength;
  Le  =targetDNA[DNA_ID]->DNALength+gene_seq;
 int si;

 bool F;
 updatecount =0;
   si =0;  

  int mpoint;
mpoint=0; 

   //cerr<< "  check new " <<endl; 
   if(me)     
   {
 mutation  =10; 
   Mutate  (me,mpoint,2);
   }
//cerr<< "  check new " <<endl;  

     F= false;  
      if(me)
	  {
   while(si<=(Mutle-3)&&si<=(Le-103)&&!F)
{


                if(genes[si]==0) 
                        F=true; 
                else 
                        F=false; 
                if(genes[si+1]==1&&F) 
                        F=true; 
                else 
                        F=false; 
                if(genes[si+2]==0&&F)        
                        F=true; 
                else 
                        F=false; 
                if( genes[si+3]==1&&F) 
                { 
                        F=true; 
						si=si+3;                      						
                }else 
 F=false; 
             si++;
			 
if(F) 
 {
int Mutle2=0;
Mutle2 =(genes[si+4]+genes[si  + 5]*4+genes[si+6]*4*4)%40;
Mutle2=(Mutle2+10)*10; 
if(Le>=(Mutle2+si+16))
{
if(genes[Mutle2+si+5]==3)
{
 Mutle2  =genes[Mutle2+si+6]*6+  Mutle2 +1; 
} 
  if(Le>=(Mutle2+si+16))
 {
if(si>0)
{
for(int i=0;i<si;i++)
 {
      updateDNA[i] =genes[i];
 }
if(type==2&&Le>(Mutle2+si+16))
{
 vector<int> dat(4);
  (*D).insert((*D).begin()+location2+1  ,  dat); // check
 }
(*D)[location2][0] =replaceDNA(updateDNA,si  , (*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID);   
   newID++;
(*D)[location2][1]=startpoint;
(*D)[location2][2]=1; (*D)[location2][3]=0;
}
else
{
if(location2>=1)
{
if((*D)[location2-1][2]==0)
(*D)[location2-1][2]  =1;
else
{
 if(locationFG>=0)
(*G)[locationFG][2]=0;
}
}
else
{
if(readD)
readD=false;
else
{
if(locationFG>=0)
(*G)[locationFG][2]=0;
 }
}
}
for(int i=0;i<(Mutle2  +16);i++)
 {
      updateGene[i] =genes[si+i];
 }
 
   vector<int> dat(3);
(*G).insert((*G).begin()+location,  dat);
if(me)
(*G)[location][0]=createGene(updateGene,  (Mutle2  +16),-1, newGID, newGFID  , true);
else
  (*G)[location][0]=createGene(updateGene,  (Mutle2  +16),-1, newGID, newGFID      );
   newGID++; newGFID++; 
(*G)[location][1]=startpoint+si;
if(Le>(Mutle2+si+16))
{
(*G)[location][2]=1;
 int leftnum;
   leftnum  =Le-(Mutle2+si+16);
for(int i=0;i<leftnum;i++)
 {
      updateDNA[i] =genes[(Mutle2+si+16)+i];
 }
if(type==2)
{
if(si<=0)
{
  (*D)[location2][0] =replaceDNA(updateDNA ,leftnum,(*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2][1]  =startpoint+Mutle2+16;
  (*D)[location2][2]=1;
  (*D)[location2][3] =0;
}
else
{
 (*D)[location2][2]=1;
  (*D)[location2][3] =0;
  (*D)[location2+1][0] =replaceDNA(updateDNA,leftnum,  -1 ,newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2+1][1]  =startpoint+Mutle2+16+si;
  (*D)[location2+1][2]=1;
  (*D)[location2+1][3] =0;
}
}
if(type==1)
{
if(si<=0)
{
if(leftnum<5000)
{

(*D).erase((*D).begin()+location2);
  (*D)[location2][0] =replaceDNA(updateDNA ,leftnum ,(*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2][1]  =startpoint+Mutle2+16;
  (*D)[location2][3] =0;
 }
  else
  {
   (*D)[location2][0] =replaceDNA(updateDNA,leftnum,(*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
   (*D)[location2][1]  =startpoint+Mutle2+16;
    (*D)[location2][2]=0;
   (*D)[location2][3] =0;
  }
}
else
{
 (*D)[location2][2]=1;
  (*D)[location2][3] =0;
if(leftnum<5000)
{ 
  (*D)[location2+1][0] =replaceDNA(updateDNA  , leftnum,(*D)[location2+1][0],newID  , targetDNA [(*D)[location2 +1][0]]->DNAfamilyID); newID++;
  (*D)[location2+1][1]  =startpoint+Mutle2+16+si;
  (*D)[location2+1][3] =0;
}
else
{
   vector<int> dat(4);
  (*D).insert((*D).begin()+location2+1,  dat) ;
 for(int i=0;i<leftnum-gene_seq;i++)
 {
      updateDNA[i] =genes[(Mutle2+16+si)+i];
 }  
    (*D)[location2+1][0] =replaceDNA(updateDNA,leftnum-gene_seq,  -1 ,newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2+1][1]  =startpoint+Mutle2+16+si;
  (*D)[location2+1][2] =0;  (*D)[location2+1][3] =0;
}


}
}
}
else
{
if(type==2)
{
(*G)[location][2]=0;
if(si<=0)
(*D).erase((*D).begin()+location2);
}
if(type==1)
{
if((*D)[location2+1][2]==0)
(*G)[location][2]=1;
if((*D)[location2+1][2]==1)
(*G)[location][2]=0;  
if(si<=0)
{
(*D).erase((*D).begin()+location2);
(*D).erase((*D).begin()+location2);
}
else
(*D).erase((*D).begin()+location2+1);
}
}

  
 

} 
else
{
Ra->deleteMRe();
}
}
else
{
Ra->deleteMRe();
}
}
//cerr<< "  check new end " <<endl; 


}
if(!F)
{
   if(me)
   {//cerr<<"  cx3 " << Ra->mcount<<endl;
     (*D)[location2][3]=  targetDNA[DNA_ID]->SubCheck( genes,(*D)[location2][3]);
	  Ra->MutationRecord[  Ra->MutationRecord.size()-1][5]  = -8;   //cerr<<"  cx6 " <<  Ra->mcount<<endl; 
        return true;	 
   }
}
else
   return true; 
}
else
{
       //cerr<< "  check new  change 1 " <<endl; 
if(false)
{
      while(si<=(Mutle-3)&&si<=(Le-103)&&!F)
{


                if(genes[si]==0) 
                        F=true; 
                else 
                        F=false; 
                if(genes[si+1]==1&&F) 
                        F=true; 
                else 
                        F=false; 
                if(genes[si+2]==0&&F)        
                        F=true; 
                else 
                        F=false; 
                if( genes[si+3]==1&&F) 
                { 
                        F=true; 
						si=si+3;                      						
                }else 
 F=false; 
             si++;
			 
if(F) 
 {
int Mutle2=0;
Mutle2 =(genes[si+4]+genes[si  + 5]*4+genes[si+6]*4*4)%40;
Mutle2=(Mutle2+10)*10; 
if(Le>=(Mutle2+si+16))
{
if(genes[Mutle2+si+5]==3)
{
 Mutle2  =genes[Mutle2+si+6]*6+  Mutle2 +1; 
} 
  if(Le>=(Mutle2+si+16))
 {
if(si>0)
{
for(int i=0;i<si;i++)
 {
      updateDNA[i] =genes[i];
 }
if(type==2&&Le>(Mutle2+si+16))
{
 vector<int> dat(4);
  (*D).insert((*D).begin()+location2+1  ,  dat); // check
 }
(*D)[location2][0] =replaceDNA(updateDNA,si  , (*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID);   
   newID++;
(*D)[location2][1]=startpoint;
(*D)[location2][2]=1; (*D)[location2][3]=0;
}
else
{
if(location2>=1)
{
if((*D)[location2-1][2]==0)
(*D)[location2-1][2]  =1;
else
{
 if(locationFG>=0)
(*G)[locationFG][2]=0;
}
}
else
{
if(readD)
readD=false;
else
{
if(locationFG>=0)
(*G)[locationFG][2]=0;
 }
}
}
for(int i=0;i<(Mutle2  +16);i++)
 {
      updateGene[i] =genes[si+i];
 }
 
   vector<int> dat(3);
(*G).insert((*G).begin()+location,  dat);
if(me)
(*G)[location][0]=createGene(updateGene,  (Mutle2  +16),-1, newGID, newGFID );
else
  (*G)[location][0]=createGene(updateGene,  (Mutle2  +16),-1, newGID, newGFID      );
   newGID++; newGFID++; 
(*G)[location][1]=startpoint+si;
if(Le>(Mutle2+si+16))
{
(*G)[location][2]=1;
 int leftnum;
   leftnum  =Le-(Mutle2+si+16);
for(int i=0;i<leftnum;i++)
 {
      updateDNA[i] =genes[(Mutle2+si+16)+i];
 }
if(type==2)
{
if(si<=0)
{
  (*D)[location2][0] =replaceDNA(updateDNA ,leftnum,(*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2][1]  =startpoint+Mutle2+16;
  (*D)[location2][2]=1;
  (*D)[location2][3] =0;
}
else
{
 (*D)[location2][2]=1;
  (*D)[location2][3] =0;
  (*D)[location2+1][0] =replaceDNA(updateDNA,leftnum,  -1 ,newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2+1][1]  =startpoint+Mutle2+16+si;
  (*D)[location2+1][2]=1;
  (*D)[location2+1][3] =0;
}
}
if(type==1)
{
if(si<=0)
{
if(leftnum<5000)
{

(*D).erase((*D).begin()+location2);
  (*D)[location2][0] =replaceDNA(updateDNA ,leftnum ,(*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2][1]  =startpoint+Mutle2+16;
  (*D)[location2][3] =0;
 }
  else
  {
   (*D)[location2][0] =replaceDNA(updateDNA,leftnum,(*D)[location2][0],newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
   (*D)[location2][1]  =startpoint+Mutle2+16;
    (*D)[location2][2]=0;
   (*D)[location2][3] =0;
  }
}
else
{
 (*D)[location2][2]=1;
  (*D)[location2][3] =0;
if(leftnum<5000)
{ 
  (*D)[location2+1][0] =replaceDNA(updateDNA  , leftnum,(*D)[location2+1][0],newID  , targetDNA [(*D)[location2 +1][0]]->DNAfamilyID); newID++;
  (*D)[location2+1][1]  =startpoint+Mutle2+16+si;
  (*D)[location2+1][3] =0;
}
else
{
   vector<int> dat(4);
  (*D).insert((*D).begin()+location2+1,  dat) ;
 for(int i=0;i<leftnum-gene_seq;i++)
 {
      updateDNA[i] =genes[(Mutle2+16+si)+i];
 }  
    (*D)[location2+1][0] =replaceDNA(updateDNA,leftnum-gene_seq,  -1 ,newID  , targetDNA [(*D)[location2][0]]->DNAfamilyID); newID++;
  (*D)[location2+1][1]  =startpoint+Mutle2+16+si;
  (*D)[location2+1][2] =0;  (*D)[location2+1][3] =0;
}


}
}
}
else
{
if(type==2)
{
(*G)[location][2]=0;
if(si<=0)
(*D).erase((*D).begin()+location2);
}
if(type==1)
{
if((*D)[location2+1][2]==0)
(*G)[location][2]=1;
if((*D)[location2+1][2]==1)
(*G)[location][2]=0;  
if(si<=0)
{
(*D).erase((*D).begin()+location2);
(*D).erase((*D).begin()+location2);
}
else
(*D).erase((*D).begin()+location2+1);
}
}

  
 

} 

}
  //cerr<< "  check new  change 2 " <<endl;
return true;
}


}

}
  }   
 
   //cerr<< "  check new  change 5 " <<endl;  
  
  return false;
}