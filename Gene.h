/*
 * Gene.h
 *
 *  Created on: 2013/12/17 
 *      Author: Yao Yao
 */
#ifndef Gene_H_
#define Gene_H_
#include <time.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <iostream> 
#include <cmath> 
#include <iomanip> 
#include<string> 
#include <fstream> 
#include <math.h> 
#include <vector>
#include "Rule.h"
#define MaxGeneLength 1000





class Robot;

class Gene  { 
public: 
        Gene() ;
      Gene(int ID,int GfID,int Gene2ID,  int* Gs ,Rule * Ra  ,int sequenceL);
         void CheckGene(  int g,   int seq );    //return the gene product   length is the length of retun message
		  void  CheckActivation( vector<double> * ActiveRate);
		 void CMu();
		 int Mutation(Gene* t)  ; 
  void  DeleteG();
	  int Expression(int& bindings  , int seq);	
//double** CheckGene(double cis,double** template1);
 void ConnectR(  Robot * ta1 );
	//double*** CheckGene(double cis,dint* Clength;ouble*** template1);
   void Update();
   void Del();  
	//bool ChangeG(int chr,  int start, int end,int* G1, int length, bool type);   //don't use now     using for change gene in later developme    
        void UpdateL();
        void WriteL();
  int   Mut(   int pos,int cxe);		
bool GeneReg(bool enhance,double bind,int chr, int degree, double range,double adapt);
  // bool ChangeGene(int chr, int start, int end,int* G1, int length,int type);
      bool ChangeG(double adapt, int number,int chr,   int conserve,double motif);
  bool ChangeG();
	//bool CheckChance(int target,int number,int p);//radomly compare
        //void LoadG(char* file,int* Garray);//not ok to use
        void WriteG(char* file , int n);// create a genome and write it on a file 
   int GetBases(int chr);int GetTl();
 int GetGar();
  int Mutation(int rate, int chr, int start, int range  );//do singal point mutation   To num 1 equal to mutate rate 0.001% 
 int CheckIncre();
   void PrintoutGene(string &Geneseq);
 void Recorrect();
void Dgenome();







bool DuplicationF (int chr,int position, int range);
        //double CheckProduct(int* pro); //cal the weight of each link 
        void ReadGenes(char* file, int x   );// read genome from a file (file name could be added later) 
      
        //double* GetWeights(char* f1,bool w); //return a double array for the weights of each link 
  int GeneID;
  int GenefamilyID;
  int GenecID;
  int GeneLength;
 Rule * RuleA;
   bool emptygene;
   int avaNum;  int avaNum2; 
     int * Active;
  int shareNum;  //how many genes existed
  
   int lastNum;   // the existed number at last check

    Robot * targetR;   
 int* Gsequence;      //int **  subtyp;  
  int mutation  ; //int binding;

 int* product;
  int ProductConcen;
  int ProduceRate;
  bool Protein;
  
  bool RNAm;
  int Cis_num;
  int Pro_num;
  int** Regulation;  
 
   int** countArray;
  int ProBind;

};

#endif /* Gene_H_ */

