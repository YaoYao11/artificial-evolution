/*
 * Rule.h
 *
 *  Created on: 2014/02/21 
 *      Author: Yao Yao
 */
#ifndef Rule_H_
#define Rule_H_
#include <time.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <cmath> 
#include <iomanip> 
#include<string> 
#include <fstream> 
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <math.h>
#include <sys/time.h>
#include <cstdlib>
#define mrange 100
#define Spath   26   // simulation time
#define  Mo_kind  1039
#define  Bspace 500 



using namespace std; 
 
class Rule{ 
public:
 Rule()  ; 
 Rule(int t);
 int  DecideEngsave(int inputv,int laste);
 void update(int t);
 void record(int c);
 bool DecideReproduce (int inputv,int eng); 	 
 void      deleteMRe();	 
 vector< vector<int> >   MutationRecord;	
 int mcount;   
 int times;
 int * PPIrate; 
 int * signal;
 double * decayRate;
};  





  
 #endif /* Rule_H_ */
  
  