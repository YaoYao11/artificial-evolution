/*
 * Pattern.h
 *
 *  Created on: 2015/01/19 
 *      Author: Yao Yao
 */
#ifndef Pattern_H_
#define Pattern_H_
#include <time.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <cmath> 
#include <iomanip> 
#include<string> 
#include <fstream> 
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <math.h>
#include <sys/time.h>
#include <cstdlib>
#include "Event.h"
#define mrange 100
#define  Bspace 500 
 
 
class Pattern{ 
public:
   Pattern();
 Pattern  (int id,int eid1,int eid2,double cr,double a,double tra,double d,vector<Pattern*> * pl,vector<Event*> * el);
 
      void Renew (int id,int eid1,int eid2,double cr,double a,double tra,double d)  ;
  void  record   ( int c);
   int Decide();
 //int Decide(vector< bool > * ev1,vector< bool > * ev2  , int n1,int n2);
 void Update();
 void deletePattern();
  int Develop (int typ);
 int  CheckDevRec();


int top;  // how many times this pattern has been included by others
int ID;     //  10000-20000 pattern to pattern   0-10000 include event 
int EID1;
int EID2;
double credit;   
bool output;
bool updated;
vector<Event*> * Elist;


vector<Pattern*> * Plist;
int lifetime;
int pos;
int neg;
int hpos;
int hneg;
double tend;
double htend;
double aim;  // not  = 0.5 to 1
double trange;
bool empty;
double decayRate;

//   int * lostGene ;  int ** mostGene; //most conventional gene in population 
};  





  
 #endif /* Pattern_H_ */
  
  