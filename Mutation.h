/*
 * Mutation.h
 *
 *  Created on: 2013/12/17 
 *      Author: Yao Yao
 */
#ifndef Mutation_H_
#define Mutation_H_
#include <time.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <iostream> 
#include <cmath> 
#include <iomanip> 
#include<string> 
#include <fstream> 
#include <math.h> 
#include "Robot.h"
#include "DNA.h"
#include "Gene.h"
#include "Rule.h"





class Mutation{ 
public: 
      
	  Mutation();
      Mutation (Gene **gene,DNA ** dna,Robot ** robots,Rule * Ragent);
bool CheckNewGene(   vector< vector<int> > * G,vector< vector<int> > * D,int DNA_ID,int DNA_ID2  ,int  location, int location2 , int locationFG,  bool &readD, bool me);
	  void LoadGenome(int RobotID);
    void LoadGseq(int F_ID,int F_ID2,int B_ID, int typ , int st);  
	
   int replaceDNA(  int* seq  ,int length,int  ID,int DID  , int FID, bool m);
	 int createGene(int* seq, int length,int  ID,int GID  , int FID,bool m);
 int replaceDNA(  int* seq  ,int length,int  ID,int DID  , int FID);
 int Havemutation   (vector< vector<int> > * G  , vector< vector<int> > * D,vector< vector< vector<int> > > * C,vector< vector<double> > * A, bool &r1,int RobotID);  
  int MergeDNA  (vector< vector<int> > * D);
	void DepartDNA();
	int createDNA(int* sequence, int length);
	 int createGene(int* seq, int length,int  ID,int GID  , int FID);
	void deleteDNA   (int address);
	void deleteGene(int address); 
	  int CheckMut  (  vector< vector<int> > * G  , vector< vector<int> > * D,int Fr, int location2,int location,int locationAfD  , bool &readD   ); 
	  void Alter(int a, int b, int robotid);
	  void Alter(int a, int* b ,  int robotid);
	  void Alter(int* a, int b , int robotid);
	  void Alter(int* a, int * b , int robotid);
	  int CheckMut (int Fr, bool gene1);
	  int CheckMut ( int* DNAs );
	  
	  

   	void   Mutate (bool &c1, int &pos  , int t );
 int DepartDNA  (vector< vector<int> > * D); 
   int type;
  int startpoint;
	  int Le;
      int Mutle;
	  int newID;
	 int newFID;
	  int newGID;
	 int newGFID;	 
	  int Glength;
int RID;
	  int MutationLength;
      DNA** targetDNA;
	  int Dnumber;
	 int ** DNAdata;
      Gene**  targetGene;
	 int Gnumber;
	  Robot** robotlist;
	    Rule* Ra;
	  int* genes;
	   int updatecount;
	    int * updateGene;
	  double mutation;
     int createdGeneNum;
int createdDNANum;
  int * updateDNA; 

  
}; 
#endif /* Mutation_H_ */