/*
 * Robot.h
 *
 *  Created on: 2013/12/17 
 *      Author: Yao Yao
 */

#ifndef Robot_H_
#define Robot_H_
#include <vector>
#include <string.h>
#include "Rule.h"
#include "Gene.h"
#include "DNA.h"
#include "Mutation.h"
#define MaxProtein 5
#define  Mo_kind  1039
 
 
class Mutation;
class Robot
{
public:

Robot();
Robot(int x, int y,int z,int p1,int g1,int t1,  Rule * ruleAgent,  Mutation  * mut,int geneNum, int dnaNum,  int** DNAs,int ** Genes,DNA **  dna, Gene** gene, bool readD)      ;
         Robot (int x, int y,int z,int p1 ,int t1,  Rule * ruleAgent,  Mutation  * mut ,vector< vector<int> > * DNAs,vector< vector<int> > * Genes,DNA **  dna, Gene** gene,bool readD, Robot * tar);
int act(int lc, int mc,int xx,int yy);  // share with GRN and non-GRN
void Recover();
void sense( double * input);
void Setfit(int v);
//bool Crobot();

void Makemove( int cost);
void  SetZ  (int z);
int Attack  (  Robot* tag);
void  SetCooperate(int totaldef,int cop);
  int Cooperate();
    int Interact();
void Eat(int food);
void DeRobot();
int   Gety();
int Getx();
bool Reproduce();
int CreatNewRobot(Robot* tar, int z);
void PPI();
void GRNcircle (int xr,int yr, int xe,int ye);
bool Checklaststep   (double &totalMu);
int GetID();
int GetID(int g);
bool   Getdock(Robot* o1);
bool Checkcurrent(int t);
int Getswam(int ta,int fid);
Robot* replicate(int x, int y, int id1,int t2);
void Clean1();
int Reorg(double v);
int Rrobot(int L1);
void Changefit(double v);
int* Delrobot();
void Addfit(double fi1);
double Getfit();
double Getdef();
int Getdockv();
int   Crobot();
void SetnextR(Robot  * robot,bool dif);
void SetlastR(Robot  * robot,bool dif);
void Refresh();
void Respond();
void Rnumch(int c1);
void Copyvote(double* v19);
void WGD();
void EWGD(Robot * rob);
void Write1( int robotId );  
void Write3( int robotId );
void Write2( int robotId ); 
void   Checkup();
void Urnum(int number);
void WriteGenome( int robotId,int o_gnum,  int o_dnum); 
void Foodtakew();
void Wincount();
double * Mvote(bool c); 
int  Dcount();
int GetID(bool q);
int GetExpressv();
inline int  CheckBind(int motif1,  int motif2, int con); 
 
 

Rule* interact;
bool readDNA;
bool empty; 
 bool defender; 
bool attacker;
   int energysave;
  bool Rep;
bool attacked; // attacked 
bool cooprater;   // cooperate  attack singnal only means  possibility  not decide
int coopraNum;
double* sen;
Mutation* M;
double expressh;
 int Atime;
double Complex;  //the number of binding at one time 
int Complex2; // how many kind of TFs are evolved in GRN at the time
double Complex3;  //final complexity
 int Duplicate;

Robot * targets;
DNA ** dnas;
Gene ** genes ;  //  use gloable gene pointer 
int ID;
 //int gneration; 
  int TotalE; //  earn energy for last step
 int fightEarn;  // energy earn from win a fight, the part of others
int preyE;  // energy earn based on prey
int energyCost;
 
 int energyCostn;
 int energyCostl;
int lifetime;  int energy;
vector< vector<int> > genome;int GenomeLength;
vector< vector<int> > genehis;
vector< vector<int> >   DNAgen;
int DNALength;
int* totalenv;
int** environment;
vector< vector< vector<int> > >   Cregulation; 
 vector< vector<double> > ActiveRate;
int lastout2; 
 
  int generation;
int attackv;
int defencev;
int cordefen;
int x1; int y1 ;   // destination of move
int xc;   int yc;  int zc;
   int totalRNA;

int SequenceLength; 
int* output;  
 int Mucount;
   int HMucount;
  int CMucount;
  int preyt;
  int attackt;   //   win attack in all fight  last step  + rep win  - rep lose or not w
 int defendt;     // win defend in all fight last step
  int cooperatet;   // only benfical count  no -  

   int lc; int ct;      int mc; int* moutput;//move output signals
 
 int rnum;
  bool moved;
  vector<double> vote;
   vector<int> vote1;
//int* nodir;
//double teamDf;
double Murate;
char* files;
double foodtake;

};
   #endif /* Robot_H_ */
