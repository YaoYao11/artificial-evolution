/*
 * Main.cc
 *
 * Created on: 2013/11/22 
 *   Author: Yao
 */
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <math.h>
#include <sys/time.h>
#include "Robot.cpp"
#include "DNA.cpp"
#include "Mutation.cpp"
#include "Gene.cpp"
#include "Rule.cpp"
#include "Event.cpp"
#include "Pattern.cpp"
#define MaxDnaNum 60000
#define MaxGeneNum 60000
#define range 1000// map range
#define MaxRobotNum 90000 // maxium robot number
#define Time 20000   // simulation time
#define ev  5   //    print output  every   n timestep
#define MGenomeLength 5000
#define MaxR 11  // maxium robot number in one cell
#define initialRobot 200
#define Maxfood_energy 20000
#define patSize 10
#define Increase_energy 300
#define Replication_mini 20000
#define tagNum 100
using namespace std;
void PrintGenome(int genen,  int dnan  , int** DNAs,int ** Genes , DNA **  dna, Gene** gene, bool readDNA1)
{  
    stringstream ss;
    int gc=0; 
	int dc=0; 
	bool end =  true;
    string buf="";
    string file="result/Print_Gec.txt";
    ofstream out(file.c_str(),ios::trunc);
        if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 			
 	while(end)
	{
		buf="";		
		if(readDNA1&&(dc<dnan))
		{
			
			dna[dc]->PrintoutDNA(buf);
			out<<buf<<"D"<<dc<<"*"<<endl;
			dc++;
			if(DNAs[dc-1][2]==0)
				readDNA1 =true;
			if(DNAs[dc-1][2]==1)
				readDNA1 = false;
				
		}
		  else
	       {
	       	    if(!readDNA1&&(gc<genen))
	       	     {
	       	     	    
	                gene[gc]->PrintoutGene(buf);	                
					out<<buf<<"G"<<gc<<"*"<<endl;
					gc++;
					if(Genes[gc-1][2]==0)
						readDNA1 =false;
					if(Genes[gc-1][2]==1)
						readDNA1  =true;
		         }
		     else
		     	 end  = false;
			}
	}	
 out.close();
}

void SetConditions(char* file1,int * condit)
{
	fstream p_stream; 
    p_stream.open(file1, ios::in); 
    if(!p_stream.is_open())
	{ 
        cerr<<"error file2"<<endl; 
    } 
				
	string str;
    getline(p_stream, str);
    int L10=atoi(str.c_str());
    condit[0] = L10;
	for(int t=1;t< condit[0];t++) 
	{
		getline(p_stream, str);				
		L10=atoi(str.c_str());
		condit[t]=  L10;
  
    }
p_stream.close();  
}
int CopyGenome(char* file, Mutation * Mag, int** DNAs,int ** Genes , DNA **  dna, Gene** gene  ,Rule * Ra,bool &readDNA1)
        { 
                fstream Genestream;
                int  *   buffer;
                Genestream.open(file, ios::in); 
                bool readDNA;  
				readDNA=true;
                int tbase; 
				tbase=0;
                if(!Genestream.is_open())
				{ 
                        cerr<<"error file"<<endl; 
                } 
                bool gc=true; 
                int last=0;    
                int i=0;  				         
				while(gc)
				{ 			
				char* str=new char [7000]; 
				if(Genestream.getline(str,7000)) 
				{
					char* strID=new char [10]; 
					buffer =new int  [7000]; 
					for( i=0; i<7000; i++) 
                                {                      	  
                                        if(str[i]=='0') 
                                         buffer[i]=0;	
                                        if(str[i]=='1') 
                                         buffer[i]=1; 
                                        if(str[i]=='2') 
                                         buffer[i]=2; 
                                        if(str[i]=='3') 
                                         buffer[i]=3;
                                       if(str[i]=='D')
                                       {
                                       	       dna[Mag->createdDNANum]=new DNA(Mag->createdDNANum,Mag->createdDNANum, Mag->createdDNANum,i,buffer,Ra);
                                       	       if(readDNA) 
                                       	       {
                                       	       	     readDNA1=true;
                                       	       	     readDNA=false;
                                       	       }
                                       	       dna[Mag->createdDNANum]->shareNum=0;
                                               DNAs[Mag->createdDNANum][0]=Mag->createdDNANum;DNAs[Mag->createdDNANum][1]=tbase-i;  //   start point on the seq
											   if( Mag->createdDNANum>=1&&last==0)
												   DNAs[Mag->createdDNANum-1][2]=0;
											   if(Mag->createdGeneNum>=1&&last==1)
												   Genes[Mag->createdGeneNum-1][2]=1;
											   last =0;
											   i=7900;
											   Mag->createdDNANum++;
                                       }
									   if(str[i]=='G')
                                       	   {
											   gene[Mag->createdGeneNum]=new Gene(Mag->createdGeneNum,Mag->createdGeneNum ,Mag->createdGeneNum  ,buffer,Ra,i); 
                                       	     if(readDNA) 
                                       	       {
                                       	       	     readDNA1=false;
                                       	       	     readDNA=false;
                                       	       }
                                             gene[Mag->createdGeneNum]->shareNum=0;
											 Genes[Mag->createdGeneNum][0]=Mag->createdGeneNum;Genes[Mag->createdGeneNum][1]=tbase-i;
											 if(Mag->createdDNANum>=1&&last==0)
												 DNAs[Mag->createdDNANum-1][2]=1;
											 if(Mag->createdGeneNum>=1&&last==1)
												 Genes[Mag->createdGeneNum-1][2]=0;
											 last =1;                                 	       
                                       	     i=7900;
                                       	     Mag->createdGeneNum++;
											 }
											 tbase++;
                                } 
 

                       //tested just perfectly read the character
                        if(Genestream.peek()==EOF) 
                        { 
                            gc=false;                        
                        } 

			    }
					
				} 
				Genestream.close();  
				Mag->newGID= Mag->createdGeneNum; 
				Mag->newID=Mag->createdDNANum;
				return   tbase; 
        } 

int CreatGenome(int genomelength,int &genen,  int &dnan  , int** DNAs,int ** Genes , DNA **  dna, Gene** gene  ,Rule * Ra,bool &readDNA1)
{
int l1=0;int * buffer;   
bool have; 
bool readDNA;
int j;  
int stop;
int cn;  
int ch;      
int  * Genebuf;  
buffer =new int  [3000];  
Genebuf =new int  [1000];   
have  = false;  
readDNA  = true;
int last;
last  = 10;	  
// l1 the current agent position on the sequence	  
while(l1<(genomelength-3000))
{
	have=false;readDNA=true;
    stop=0;
    j=0;
    srand((unsigned)time(NULL)+rand());
    //initialize buffer
    for(int i=0; i<3000; i++)
	{ 
        buffer[i]=-1;
    }
	for(int i=0; i<3000; i++)
	{
		buffer[i]=rand()%4;
		if(i>=3)
		{
			if(buffer[i-3]==0&&buffer[i-2]==1&&buffer[i-1]==0&&buffer [i]==1&&!have) //check if there is a promoter
			{
				have=true;
				stop  = i-3; //mark the start point of the gene on the sequence
				if(stop==0)
					readDNA  =false; //no dna update
            }
			if(have)  //if there is promoter motif on the sequence, find the gene
			{
				//initialize gene buffer
				for(int z=0; z<1000; z++)
				{ 
			        Genebuf[z]=-1;
				}  

    Genebuf[0]=0;
	Genebuf[1]=1;
	Genebuf[2]=0;
    Genebuf[3]=1;
    j  = 4; // mark the current point
	cn  = 9; //
    ch  = 10;
	while(((l1+i+1)<(genomelength-1000))&&j<cn)
	{
		Genebuf[j]=rand()%4; //produce the genetic code
		if(j==8)
		{
			cn=Genebuf[4]+Genebuf[5]*4+Genebuf[6]*4*4; 
			cn=cn%40; //  cis region number=63%40  
			ch=cn*10+110; // cis region finished point
			cn=cn*10+120; //proposed gene length  
		}
		if(j==ch)
        {
			if(Genebuf[j-1]==3) //if it is regulatory gene
			{
				cn=cn+Genebuf[j]*6+1;   // new gene length if there is more than one product
            }
        }
		j++;
    }
	if((l1+i+1)>=(genomelength-1000))
	{
		have  = false;
	}
	} // end if (have)
}
if(have)
{
buffer[i]=-1;
buffer[i-1]=-1;
buffer[i-3]=-1;
buffer[i-2]=-1;
i=3700;
}
else
stop=0;
}
if(stop==0)
{
 stop=3000;
} 
if(last==10&&readDNA)
{ 
 readDNA1=true;
} 
if(last==10&&!readDNA)
{ 
 readDNA1=false;
}
if(readDNA)
{
dna[dnan]=new DNA(dnan,dnan, dnan,stop,buffer,Ra); 
dna[dnan]->shareNum=0;
DNAs[dnan][0]=dnan;DNAs[dnan][1]=l1;  //   start point on the seq
if( dnan>=1&&last==0)
DNAs[dnan-1][2]=0;
if(genen>=1&&last==1)
Genes[genen-1][2]=1;
last =0;
l1=l1+stop;
dnan++;
}
if(have)
{
gene[genen]=new Gene(genen,genen  ,genen  ,Genebuf,Ra,cn);
gene[genen]->shareNum=0;
Genes[genen][0]=genen;Genes[genen][1]=l1; 
if(dnan>=1&&last==0)
DNAs[dnan-1][2]=1;
if(genen>=1&&last==1)
Genes[genen-1][2]=0;
last =1;
l1=l1+cn;
genen++;
}
}
 return l1;
}
  
inline int CreatRobot(Robot ** robots , int * status,int ID1, int ID2,int z)
{
  robots  [ID1]  -> CreatNewRobot(  robots  [ID2],z );
status  [ID2] = 1; 
  return ID2;
}

inline int CheckB(int x)
{
if(x>999)
x  = x -range;
if(x<0)
x  = range+x; return x;
}


int main(int argc, char **argv)
{
int Lc  = 20;   //energy cost for leaving
int Mc  =30;   //energy cost for move
int time1=0;  // represent current time
int envrec=0; 
int envcha=0;
bool mored=true;
int o_gnum=0;
int o_dnum=1;
int seed =0;
int seed2=0;
int MuRNum;
int chx1;   //  cin tag
bool changed=false;
int xmr;
int ymr;
int xme;
int yme;
int localNum;
int createdRobotNum;
int totalRobotNum;
createdRobotNum=  0; 
totalRobotNum= 0;
int timestep=0;
double Mutral=0;
int Mtime  =0;  		
double HMutral  = 0;  
double HaM=0;
int ** search =new int* [tagNum];
for(int h=0;  h<tagNum; h++)
{
  search[h] =new int [tagNum];
} 
char * file =new char[27];
file ="condition1.txt";
int * se1=new int [10];
SetConditions(file,se1);
file ="condition2.txt";
int * se2 = new int [10];
SetConditions(file,se2);
Robot ** robots=new Robot*[MaxRobotNum];
Gene ** gene = new Gene * [MaxGeneNum];
DNA ** dna  =new DNA*  [MaxDnaNum];
Rule * Ragent  = new Rule (timestep);
Mutation * Magent  = new Mutation(gene,dna,robots,Ragent);
int * status=new int[MaxRobotNum];   
double * input=new  double [4];    // environment information
int ** local=new int* [MaxR-2]; 
int dnan1; 
int genen1;  
dnan1  = 0;   
genen1  = 0;
bool reDNA;
int ** GeneSeq;
int ** DNASeq;
vector< Event * > Elist;
vector< Pattern * >  Plist;
vector< int[patSize] >  Co; 
vector< int[patSize] >  PCo;
GeneSeq=new int* [MGenomeLength];
bool test1;
test1=true;
bool test2;
test2 =true;
bool test3;
test3 =true;
bool test5;
test5=true;
bool test6;
test6 =true;
Event * e1=new Event(robots,gene,Ragent,se1[se1[0]-1],&se1[0],se1[0],300,0.3);
Elist.push_back(e1);
e1  = new Event(robots,gene,Ragent,se2[se2[0]-1],&se2[0],se2[0],300,0.36);
Elist.push_back(e1); 
bool test7;
test7 =true;
//initialize all gene and DNA    
DNASeq=new int*[MGenomeLength];
for(int i=0; i<MGenomeLength; i++)
{
	GeneSeq[i]=new int[3];
	for(int h=0;  h<3; h++)
	{
		GeneSeq[i][h]=  -1;  //[0] ID; [1] length;   [2] 0 next 1 go to another; 
	}
	DNASeq[i]=new int[3];
	for(int h=0;  h<3; h++)
	{
		DNASeq[i][h]=  -1; 
	}
}
for(int i=0; i<(MaxR-2); i++)
{
    local[i] =new int  [2];
    local[i][0]=-1; 
    local[i][1]=-1; 
}
for(int i=0; i<MaxRobotNum; i++)
{
	status[i]=-1;
}


int *** map=new int** [range];
for(int m=0;m<range;m++) 
{
	map[m]=new int*[range];
	for(int i=0; i<range; i++) 
	{
		map[m][i]=new int  [MaxR];
		for(int j=0; j  < MaxR; j++) 
		{
			map[m][i][j]=-1;
		}
	}
}


 int glength;
 int gNumber; 
 chx1=1;
 if(chx1==1)
 {
	 glength=CreatGenome(100000,genen1, dnan1,DNASeq,GeneSeq,dna,gene , Ragent,  reDNA);
	 Magent->newGID= genen1; 
	 Magent->newID=dnan1;
	 Magent->createdGeneNum  =genen1; 
	 Magent->createdDNANum=dnan1;
 }
   else
   {
   	file ="result/Print_Gec.txt";
	glength=CopyGenome(file,Magent,DNASeq,GeneSeq,dna,gene,Ragent,  reDNA);
   	genen1=Magent->createdGeneNum; 
   	dnan1=Magent->createdDNANum;
   }

cerr<<"  do you want to print out the initial genome template to the file? Y=1;N=2; " <<endl;
cin>>chx1;
if(chx1==1)
PrintGenome(genen1, dnan1,DNASeq,GeneSeq,dna,gene,  reDNA);
o_gnum=genen1;
o_dnum= dnan1;


int robot=initialRobot;
int food=1000;
int fx; int fy;
// initialize food 
int interval=30;
while(food>0||robot>0)
{

  srand((unsigned)time(NULL)+rand()); 
  fx=rand()%range;
  fy=rand()%range;
  if(map[fx][fy][1]==-1||map[fx][fy][0]==-1)
  {
      int r79=rand()%9000;
	  if(r79>305&&r79<310&&map[fx][fy][1]==-1&&robot>0)
	  {
		  map [fx][fy][2]  = initialRobot-robot;
		  map [fx][fy][1]=1;  
		  robots[initialRobot-robot]=new Robot( fx,fy,2,(  initialRobot-robot),glength,0, Ragent, Magent,genen1,dnan1,DNASeq,GeneSeq,dna,gene,reDNA);
		  if(robots[initialRobot-robot]->Duplicate<1)
			  seed++;
		      seed2++;
			  robot--; 
	   }
  else
  {
	 if(r79>1&&r79<10&&map   [fx][fy][0]==-1&&food>0)
	 {
		 map   [fx][fy][0]  = 10000;
		 food--;
	  }
	}     // else
  } 

} //end while() 
cerr<<" robot and environment ready "<<endl;  
createdRobotNum=initialRobot;
totalRobotNum=initialRobot;  
// main loop
int season=0;
int attnum=0;  //number of attack
int cooper=0; //   number of cooperation
int Cost=0;
int MaxRobot;
int MaxEng;
int p1;
int p2;
int mx; 
int ix;
int my;
int iy;
int mx2; 
int ix2;
int my2;
int iy2;
int r2;
for(int i=0; i<MGenomeLength; i++)
{
delete [] GeneSeq[i];
delete [] DNASeq[i];
}
delete [] GeneSeq;
delete [] DNASeq;
cerr<<" simulation is running. Check the record in the result folder"<<endl;
while(timestep<Time)
{
	for(int m=0;m<range;m++) 
	{
		for(int i=0; i<range; i++) 
		{
			if(map[m][i][1]==-1)
				map[m][i][1]=0;
		}
	}   
    Ragent ->update(1);
	int time1 =  timestep%200;
	if(time1<50)
	{
       season=1;
       Lc=30;
       Mc=50;
    }
    else
	{
		if(time1<100)
		{
			season=2;Lc=10;Mc=40;
		}
		else
        {
			if(time1<150)
			{
				season=3;
				Lc=20;
				Mc  =50;
			}
            else
			{
				season=4;
				Lc =40;
				Mc  =70;
			}
		}   //end els

    } //end else
    if(totalRobotNum>931)
     interval++;

for(int m=0;m<range;m++) 
{
	for(int i=0; i<range; i++)  
	{
		srand((unsigned)time(NULL)+rand());
		if(map[m][i][0]>0&&map[m][i][0]<Maxfood_energy) 
		{
			if(map[m][i][0]>=(Maxfood_energy-Increase_energy))
				map[m][i][0]=Maxfood_energy;
			else
				map[m][i][0]=map[m][i][0]  + Increase_energy;
		}
		if(map[m][i][0]>=Replication_mini) 
		{
			p2= map[m][i][0]  - Replication_mini;  
			p2 =p2/50;
			r2  = rand()%100; 
			if(p2>=r2)
			{
				if(rand()%2==0)
					p1=1;
				else
					p1  = -1;
				if(rand()%2==0)
					p2=1;
				else
					p2  = -1;
				
				if((m+1)<range&&(m-1)>-1&&(i+1)<range&&(i-1)>-1)
				{
					mx=m+p1; 
                    ix=i+p2;
                    my=m-p1; 
                    iy=i-p2;
                    mx2=m+p2; 
                    ix2=i+p1;
                    my2=m-p2;
                    iy2=i-p1;
				}
				else
				{
					mx=CheckB(m+p1); 
					ix=CheckB(i+p2);
					my=CheckB(m-p1); 
					iy  =CheckB(i-p2);
					mx2=CheckB(m+p2); 
					ix2=CheckB(i+p1);
					my2=CheckB(m-p2);
					iy2  =CheckB(i-p1);
				}
				if(map[mx][ix][0]<0) 
				{ 
			       map[m][i][0] = map[m][i][0]/2;
			       map[mx][ix][0]=map[m][i][0];
			    }
				else
				{
					if(map[my][iy][0]<0)
					{
						map[m][i][0] = map[m][i][0]/2;
						map[my][iy][0]=map[m][i][0];
					}
					else
					{
						if(map[mx][iy][0]<0)
						{
							map[m][i][0] = map[m][i][0]/2;
							map[mx][iy][0]=map[m][i][0];
						}
						else
						{
							if(map[my][ix][0]<0)
							{
								map[m][i][0] = map[m][i][0]/2;
								map[my][ix][0]=map[m][i][0];
							}
							else
							{
								if(map[mx2][ix2][0]<0) 
								{
									map[m][i][0] = map[m][i][0]/2;
									map[mx2][ix2][0]=map[m][i][0];
								}
								else
								{
									if(map[my2][iy2][0]<0)
									{
										map[m][i][0] = map[m][i][0]/2;
										map[my2][iy2][0]=map[m][i][0];
									}
									else
									{
										if(map[mx2][iy2][0]<0)
										{
											map[m][i][0] = map[m][i][0]/2;
											map[mx2][iy2][0]=map[m][i][0];
										}
										else
										{
											if(map[my2][ix2][0]<0)
											{
												map[m][i][0] = map[m][i][0]/2;
												map[my2][ix2][0]=map[m][i][0];
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}  
int foodpo=0;
int rn=0;       
int fn=0;  int cost=0;
cerr<<" environmet"<<endl;
//sensing environment
for(int m=0;m<range;m++) 
{
	for(int i=0; i<range; i++) 
	{
		if(map[m][i][1]>0)
		{
			if(rand()%2==0)
				p1=1;
			else
				p1  = -1;
			if(rand()%2==0)
				p2=1;
			else
				p2  = -1;
			if((m+1)<range&&(m-1)>-1&&(i+1)<range&&(i-1)>-1)
            {
				mx=m+p1;ix=i+p2;my=m-p1; iy=i-p2;mx2=m+p2; ix2=i+p1;my2=m-p2;iy2=i-p1;
			}
			else
			{
				mx=CheckB(m+p1); ix=CheckB(i+p2);my=CheckB(m-p1); iy  =CheckB(i-p2);mx2=CheckB(m+p2); ix2=CheckB(i+p1);
				my2=CheckB(m-p2);iy2  =CheckB(i-p1);
			}
			if(map[mx][ix][1]>0)
				input[0] = map[mx][ix][1];
			else
				input[0]=0;
if(map[my][iy][1]>0)
input[0]=input[0]+map[my][iy][1];
if(map[mx2][ix2][1]>0)
input[0]=input[0]+map[mx2][ix2][1];
if(map[my2][iy2][1]>0)
input[0]=input[0]+map[my2][iy2][1];
if(map[mx][ix2][1]>0)
input[0]=input[0]+map[mx][ix2][1];
if(map[my][iy2][1]>0)
input[0]=input[0]+map[my][iy2][1];
if(map[mx2][ix][1]>0)
input[0]=input[0]+map[mx2][ix][1];
if(map[my2][iy][1]>0)
input[0]=input[0]+map[my2][iy][1];
if(map[m][i][1]>0)
input[0]=input[0]+map[m][i][1];
if(map[mx][ix][0]>0)
input[2] = map[mx][ix][0];
else
      input[2]=0;
if(map[my][iy][0]>0)
input[2]=input[2]+map[my][iy][0];
if(map[mx2][ix2][0]>0)
input[2]=input[2]+map[mx2][ix2][0];
if(map[my2][iy2][0]>0)
input[2]=input[2]+map[my2][iy2][0];
if(map[mx][ix2][0]>0)
input[2]=input[2]+map[mx][ix2][0];
if(map[my][iy2][0]>0)
input[2]=input[2]+map[my][iy2][0];
if(map[mx2][ix][0]>0)
input[2]=input[2]+map[mx2][ix][0];
if(map[my2][iy][0]>0)
input[2]=input[2]+map[my2][iy][0];
if(map[m][i][0]>0)
input[2]=input[2]+map[m][i][0]; 
if(map[mx][ix][1]>map[my][iy][1])
{
MaxRobot=map[mx][ix][1];
xmr=mx;  
ymr=ix;
}
else
{
MaxRobot=map[my][iy][1];
xmr=  my;  
ymr= iy;
}
if(MaxRobot<map[mx2][ix2][1])
{
MaxRobot=map[mx2][ix2][1];
xmr=mx2;  
ymr=ix2;
}
if(MaxRobot<map[my2][iy2][1])
{
MaxRobot=map[my2][iy2][1];
xmr=  my2;  
ymr= iy2;
}
if(MaxRobot<map[mx][ix2][1] )
{
MaxRobot=map[mx][ix2][1];
xmr=mx;  
ymr=ix2;
}
if(MaxRobot<map[my][iy2][1])
{
MaxRobot=map[my][iy2][1];
xmr=  my;  
ymr= iy2;
}
if(MaxRobot<map[mx2][ix][1])
{
MaxRobot=map[mx2][ix][1];
xmr=mx2;  
ymr=ix;
}
if(MaxRobot<map[my2][iy][1])
{
MaxRobot=map[my2][iy][1];
xmr=my2;  
ymr=iy;
}
if(MaxRobot<map[m][i][1])
{
MaxRobot=map[m][i][1];
xmr=m;  
ymr=i;
}
if(MaxRobot<1)
         MaxRobot=1;

MaxRobot = MaxRobot*100; int ARobot  = (int)((input[0]/9.0)*100);

if(ARobot>1)
input[1]  = MaxRobot/(ARobot*1.0);
else
input[1]  = 5;
 
if(map[mx][ix][0]>map[my][iy][0])
{
MaxEng=map[mx][ix][0];
xme=mx;  
yme=ix;
}
else
{
MaxEng=map[my][iy][0];
xme=my;  
yme=iy;
}
if(MaxEng<map[mx2][ix2][0])
{
MaxEng=map[mx2][ix2][0];
xme=mx2;  
yme=ix2;
}
if(MaxEng<map[my2][iy2][0])
{
MaxEng=map[my2][iy2][0];
xme=my2;  
yme=iy2;
}
if(MaxEng<map[mx][ix2][0] )
{
MaxEng=map[mx][ix2][0];
xme=mx;  
yme=ix2;
}
if(MaxEng<map[my][iy2][0])
{
MaxEng=map[my][iy2][0];
xme=my;  
yme=iy2;
}
if(MaxEng<map[mx2][ix][0])
{
MaxEng=map[mx2][ix][0];
xme=mx2;  
yme=ix;
}
if(MaxEng<map[my2][iy][0])
{
MaxEng=map[my2][iy][0];
xme=my2;  
yme=iy;
}
if(MaxEng<map[m][i][0])
{
MaxEng=map[m][i][0];
xme=m;  
yme=i;
}
if(MaxEng<0)
MaxEng=0;
if((input[2]/9.0)>1)
input[3]  = (int)(MaxEng/(input[2]/9.0));
else
{
if(input[2]<=0)
   input[3] =0;
   else
     input[3] =6;
   }   
for(int j=0; j<(MaxR-2); j++) 
{  
 if(map[m][i][j+2]>=0)
  {
   robots[map[m][i][j+2]]->sense(input);   
 
   robots[map[m][i][j+2]]->GRNcircle(xmr,ymr,xme,yme);
    }
}  //end j
}
}
}
cerr  <<"  check record "<<endl;
if(timestep%ev==0)
{

  bool checkone  = true;	
	
  int count1  = 0;
  while(checkone&&count1<100)
         {	
int  crb=rand()%createdRobotNum   ; 
   if(!robots[crb]->empty)
   { 
  checkone=false;
 }
else
  count1++;
}

int Max=0;
int Minis=0;
int compare  =0;
int Mge=0;

int Wge=0;

for(int i=0; i<Magent->createdGeneNum;i++)
{
if((!gene[i]->emptygene)&&(gene[i]->shareNum>1))
{
   compare = gene[i]->CheckIncre();
if(Max<=compare)
{
  Max=compare; Mge=i; 
}
if(Minis>=compare)
{
  Minis =compare; Wge=i; 
} 
   }
}
cerr<<" increased gene "<< Mge<<"  increased " <<Max<<" dzcreased gene "<<Wge<<"   decrease " <<Minis<<endl;
}
int Ceng;
int contribute;
int xm;
int ym;
int cooperNum;
for(int m=0;m<range;m++) 
{
for(int i=0; i<range; i++)  
{
if(map[m][i][1]>1)
{

   cooperNum =0;
  Ceng  =0; 
for(int h=0;h<(MaxR-2);h++)
{
local[h][0]=-1; local[h][1]=-1;
}
for(int j=0; j<(MaxR-2); j++)
{
if(map[m][i][j+2]>=0)
{
contribute=robots[map[m][i][j+2]]->Cooperate();
Ceng=Ceng+contribute; 
  if(robots[map[m][i][j+2]]->cooprater)
     cooperNum++;
}
}

for(int j=0; j<(MaxR-2); j++)
{
if(map[m][i][j+2]  >=0)
robots[map[m][i][j+2]]->SetCooperate(Ceng,cooperNum);
}

localNum=0;
for(int j=0; j   <(MaxR-2); j++)
{
local[j][0]=map[m][i][j+2];
 if(map[m][i][j+2] >=0)
  local[j][1] =robots[map[m][i][j+2]]->Interact();  //  0 cooperation 1 attack  2 not avaiable
  else
  local[j][1] =2; 
   if(local[j][1]<2&& local[j][1]>=0)
    localNum++;  
}

bool loopc   =true;
int ranID; 
int cc=0;
bool foodwin=false;  
int winner; 
winner=-1;
while(localNum>1&&loopc)
{
for(int j=0;j<(MaxR-2);j++)
{
if(local[j][1]==1)
{
if(test1)
{
test1=false;
}  
ranID =rand()%(MaxR-2);
 
while ((j==ranID||local[ranID][1]>=2)&&cc<=66)
{
  ranID =rand()%(MaxR-2);   
cc++;
   }
if(cc>66)
   loopc  =false;
else
{
int ack ;
   if(test3)
{
test3=false;
}

ack  = robots[local[j][0]]->Attack(robots[local[ranID][0]]);
if(ack==1)
 {
 map[m][i][ranID+2]  =-1;
 local[ranID][1]  =2; 
 localNum--;
 map[m][i][1]--;
 totalRobotNum--;
 }
if(ack==2)
 {
robots[local[j][0]]->DeRobot();
 map[m][i][j+2]  =-1;
 local[j][1]  =2;
 localNum--; map[m][i][1]--;
 totalRobotNum--;
//cerr<<" ok3 " <<endl ;
 }
if(ack>=3)
 {
 foodwin=true;
 if(ack==3)
 {
 winner= local[j][0];
   local[j][1]=0;
 }
 if(ack==4)
 {
  local[j][1]  =2;
  localNum--;
  winner=local[ranID][0]; 
 }  
 }
if(ack<=0)
 {
  local[j][1]  =2;
 localNum--;
 } 
loopc  =true;
j  =100;
}
}
else
{
loopc  =false;
}
}
}
if(map[m][i][0]>0&&localNum>0)
{
if(winner==-1&&foodwin)
	foodwin=false;
    int foodE;
if(foodwin)
	foodE=map[m][i][0];
else
	foodE = map[m][i][0]/localNum;
for(int j=0; j   <(MaxR-2); j++)
{
	if(foodwin)
	{
		if(map[m][i][j+2]==winner)
		{
			robots[map[m][i][j+2]]->Eat(foodE);
			j=998;
		}
	}
	else
	{
		if(map[m][i][j+2] >=0)
			robots[map[m][i][j+2]]->Eat(foodE);
	}
}
map[m][i][0]=0;
}
for(int j=0; j   <(MaxR-2); j++)
{
	if(map[m][i][j+2] >=0)
	{
		xm=robots[map[m][i][j+2]]->Getx();    // one robots only can move once at one time
		ym  =robots[map[m][i][j+2]]->Gety();
		if(xm>=0&&ym>=0&&map[xm][ym][1]<(MaxR-2)&&  !( xm==m&&ym==i))
		{
			robots[map[m][i][j+2]]->Makemove(Mc);    //  change xm ym -1
			for(int h=2;h<MaxR;h++)
			{
				if(map[xm][ym][h]==-1)
				{
					if(test6)
					{
						test6=false;
					}
					robots[map[m][i][j+2]]->SetZ(h);
					map[xm][ym][h] =map[m][i][j+2];  
					map[m][i][j+2]=-1;
					map[xm][ym][1]++;  
					map[m][i][1]--;  
					h=91;
				}
			}
		}
	}
}
}
else
{
	if(map[m][i][1]==1)
	{  
       if(map[m][i][0]>0)
	   {
		   for(int j=0; j   <(MaxR-2); j++)
		   {
			   if(map[m][i][j+2] >=0)
			   {
				   robots[map[m][i][j+2]]->Eat(map[m][i][0]);  
				   j = 99;
			   }
		   }
		   map[m][i][0]  =0; 
		}
		for(int j=0; j   <(MaxR-2); j++)
		{         
	       if(map[m][i][j+2] >=0)
		   {
			   xm=robots[map[m][i][j+2]]->Getx();    // one robots only can move once at one time
			   ym  =robots[map[m][i][j+2]]->Gety(); 
			   if(xm>=0&&ym>=0&&map[xm][ym][1]<(MaxR-2)&&  !( xm==m&&ym==i)  )
			   {
				   robots[map[m][i][j+2]]->Makemove(Mc);    //  change xm ym -1
				   for(int h=2;h<MaxR;h++)
				   {
					   if(map[xm][ym][h]==-1)
					   {
						   if(test7)
						   {
							   test7=false;
						   }
						   robots[map[m][i][j+2]]->SetZ(h);
						   map[xm][ym][h] =map[m][i][j+2];  
						   map[m][i][j+2]=-1;map[xm][ym][1]++;
						   map[m][i][1]--; 
						   h=91;
						   j=99;
					   }
				   }
			   }
			}
		}
	}
}
}
}
Mutral  = 0; 
MuRNum=0;
for(int j=0; j   <createdRobotNum; j++ )
 { 
   if(!robots[j]->empty)
    { 
        robots[j]->Atime  = timestep;	
	   robots[j]->Atime++ ;
      if(robots[j]->Checklaststep(Mutral))
	    MuRNum++;	  
   bool derobot; 
  derobot  =false;
 if(false)
 {
  robots[j] ->DeRobot();
  derobot  = true;
   }
else
  {   
     if( robots[j]->energy < 100)
	 {
		 robots[j] ->DeRobot();
		 derobot  =  true ;
	 }
  }
if(derobot)
{
   int x= robots[j] ->xc; int y= robots[j]->yc; 
   map[x][y][1]--;
   totalRobotNum--;
   map[x][y][robots[j]->zc]=-1;
}  
	}
}

for(int m=0;m<range;m++) 
{
	for(int i=0; i<range; i++)  
	{
	if(map[m][i][1]>0)
	{
		for(int j=0; j<(MaxR-2); j++)
		{
			if(map[m][i][j+2]>=0&&robots[map[m][i][j+2]]->Reproduce()&&map[m][i][1]<(MaxR-2))
			{
				bool rc;   
				for(int h=2;h<MaxR;h++)
				{
					if(map[m][i][h]==-1)
					{
						rc=true;   
						for(int k=0;k<createdRobotNum;k++)
						{ 
					      if(robots[k]->empty)
						  {
							  map[m][i][h]=CreatRobot (robots,status,map[m][i][j+2],k,h); 
							  map[m][i][1]++;
							  if(robots[k]->Duplicate<1)
								  seed++;
							      seed2++;
								  h=91; 
								  totalRobotNum++;
								  k =MaxRobotNum+1;      
								  rc=false;
						  }
						}
						if(rc)
						{
							map[m][i][h]  = createdRobotNum; 
							robots[createdRobotNum]=new Robot(m,i,h,createdRobotNum,timestep, Ragent, Magent,&(robots[map[m][i][j+2]]->DNAgen),&(robots[map[m][i][j+2]]->genome),dna,gene,reDNA,robots[map[m][i][j+2]]); 
							robots[createdRobotNum]->GenomeLength  = Magent->Havemutation(&(robots[createdRobotNum]->genome),&(robots[createdRobotNum]->DNAgen),&(robots[createdRobotNum]->Cregulation),&(robots[createdRobotNum]->ActiveRate), robots[createdRobotNum]->readDNA,createdRobotNum); 
							robots[createdRobotNum]-> DNALength=robots[createdRobotNum]->DNAgen.size(); //cerr<<" create  new  c1 :"<<   createdRobotNum<<endl; 
							if((rand()%5)<=2&&mored)  
								robots[createdRobotNum]->WGD(); 
							if(robots[createdRobotNum]->Duplicate<1)
								seed++;
							seed2++;
							map[m][i][1]++;
							createdRobotNum++;
							totalRobotNum++;
							h=91;
						}
					}
				}
			}
		}
	}
}
}

if(MuRNum!=0)
{
Mutral  = Mutral /MuRNum;
   HMutral=HMutral+Mutral;
 Mtime++;
}
if(Mtime>0)
{
HaM  = HMutral/Mtime;
}
for(int i=0; i< Elist.size(); i++)  
{

  double Dmu=0;
  double Nmu=0;
  double tDmu=0;
  double	tNmu=0;  
  double hDmu=0;
  double hNmu=0;
  double rhDmu = 0;
  double  rhNmu = 0 ;
  double  rDmu  = 0 ;
  double  rNmu  = 0 ;
  if((!Elist[i]->empty))
  {
	  Elist[i]  ->Update (Mutral);
	  for(int j=0; j   <createdRobotNum; j++ )
	  {
		  robots [j]->Checkup();
		  if((!robots [j]->empty)&&(!Elist[i]->empty))
		  {
			  if(Elist[i]->CollectData(j))
			  {
				  if(i==0)
				  {
					  if(robots [j]->Murate>Mutral)
					  {
						  Dmu++;
						  rDmu = rDmu + robots [j]->Murate;
					  }
					  if(robots [j]->HMucount/(robots [j]->SequenceLength/10000.0)> HaM)
					  {
						  hDmu++;
						  rhDmu = rhDmu + robots [j]->HMucount/(robots [j]->SequenceLength/10000.0);
					  }
					  tDmu++; 
				  }
				  if(i==1)
				  {
					  if(robots [j]->Murate>Mutral)
					  {
						  Nmu++;
						  rNmu = rNmu + robots [j]->  Murate;
					  }
					  if(robots [j]->HMucount/(robots [j]->SequenceLength/10000.0)>  HaM)
					  {
						  hNmu++;
						  rhNmu = rhNmu + robots [j]->HMucount/(robots [j]->SequenceLength/10000.0);
					  }
					  tNmu++; 
				  }
			  }	
		   }
}
 
if(tDmu  > 0)
cerr<<"   Dmu/t "<<  (double)(Dmu/tDmu) <<"  hDmu/t "<< (double)(hDmu/tDmu)<<"  rhDmu/t "<< (double)(rhDmu/tDmu)<<"  rDmu/t "<< (double)(rDmu/tDmu)<<endl;  
if(tNmu>0)
cerr<< "  Nmu/t "<< (double)(Nmu/tNmu) <<"  hNmu/t "<<  (double)(hNmu/tNmu)<<"  rhNmu/t "<<  (double)( rhNmu/tNmu)<<"  rNmu/t "<<  (double)( rNmu/tNmu)<<endl;
if((!Elist[i]->empty))
	Elist[i]  ->record(i);
}
else
{
cerr<<" i " << i <<endl;
if(i==0)
Elist[i]->Renew (se1[se1[0]-1],&se1[0],se1[0],300,0.3);
if(i==1)
Elist[i]->Renew (se2[se2[0]-1],&se2[0],se2[0],300,0.3);
}
}
int ei1=0; 
int ei2=0;
cerr<<" time :"<<   timestep<<endl;		  
if(timestep% ev==0)
{
	int out1=timestep/ev;
    stringstream ss;
    ss<<out1;
	string file="result/out" +ss.str()  +".txt";
	ofstream out(file.c_str(),ios::trunc); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 

 for(int m=0;m<range;m++) 
{
for(int i=0; i<range; i++)  
{
 if(map[m][i][0]>0&&map[m][i][1]>0)  
   out<<"&";
 if(map[m][i][0]<=0&&map[m][i][1]<=0)
   out<<" ";
 if(map[m][i][0]>0&&map[m][i][1]<=0)
   out<<"*"; 
 if(map[m][i][0]<=0&&map[m][i][1]>0  )
   out<<map[m][i][1];  
}
 out<<endl;
}					   
for(int i=0; i<createdRobotNum; i++)  
{
 if(robots[i]->empty)  
   out<<"empty poistion" <<endl;
else
{
 out<< robots[i]->energy << " Duplication: " << robots[i]->Duplicate <<endl;
}
}
out<<"total robot number "<<totalRobotNum;
out<<"   seed "<<seed;  out<<"   seed "<<seed2;
if(seed<=(seed2-seed))
{
	mored=false;	
}
out.close();
if(timestep>7000)
        return 1;
}
timestep++;
}



return 0;
}				   
					   
					   
					   
					   


