/**
 * Copyright: Yao Yao
 */ 
#include "Rule.h"
 
 
using namespace std;  
 
  Rule::Rule()
{
}

 
Rule::Rule(int t)
 {
vector<int> dat(6);
mcount=0; 
MutationRecord.push_back ( dat);   
times=t;
int interv  =Bspace/Spath;
int  interv2 =  interv%10;
srand((unsigned)time(NULL)+rand());
int  interval =  interv-interv2; 
decayRate=new double [Mo_kind];
signal = new int [Spath]; 
int dec;
for(int i=0;i< Mo_kind; i++)
{
   dec  = rand()%67;
   decayRate [i] = dec/100.0;    
}

for(int i=0;i< Spath; i++)
{
 signal[i]=i*interv+rand()%interval;   
 i++;  
 signal[i]=i*interv+rand()%interval+500;      
}

}  
int   Rule::DecideEngsave(int inputv,int laste)
{
	double le=laste*1.5;
	double cce  =inputv;
	cce=cce/le;
  if(cce>=1)
  	  return 9;
  else
  	  {
  	  	   int cx=9;
  	  	  while(cx>0){
  	  	  	  
  	    if(cce>=  (cx*0.1))
  	     {
  	     	      return cx;
  	     }	     
  	      cx--;
  	  	  }
  	  	return 0;
  	  } 
 return 0;
}

void Rule::update   (int t)
{


  int out1=times;
       stringstream ss;
       ss<<out1;

               times++;	   

	
   string file="result/report___at_" +ss.str()  +".txt";
    ofstream out(file.c_str(),ios::trunc); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 			   
   
  out<<"  mutation  num " <<mcount<<endl;   mcount=0;
for(int m=0;m<MutationRecord.size();m++) 
{
for(int i=0; i<6; i++) 
{
 out<< MutationRecord[m][i]<<" ";
   MutationRecord[m][i] =-1;
			}
out<<endl;
}
 		
  out.close();
}
 void Rule::deleteMRe()
{
     mcount--;
   MutationRecord.erase(MutationRecord.end());
}
void Rule::record   ( int c)
{
}

bool  Rule::DecideReproduce (int inputv,int eng)
{
	int range  = inputv*2;
	int levelc= range/10;
	int dif=0;
	dif  =(eng-18000)/2;
	srand((unsigned)time(NULL)+rand());
	int dx  = rand()%100;
	if(levelc>=1)
	{
		 
		 if(dif>=0)
		 {
		 	 dif=dif/levelc;
		 	 if(dif>=5)
		 	 	 return true;
		 	 else
		 	 {
		 	 	 dif=50+10*dif;
		 	 	 if(dif>=dx)
		 	 	 	 return  true;   //ture;
		 	 	   else
		 	 	   	   return false;
		 	 	 
		 	 }
		 	 
		 }
		       else
		   {
		 	 dif=(dif/levelc)*-1;
		 	 if(dif>=5)
		 	 	 return false;
		 	 else
		 	 {
		 	 	 dif=50-10*dif;
		 	 	 if(dif>=dx)
		 	 	 	 return  true;   //txq
		 	 	  else
		 	 	  	  return false;
		 	 	  
		 	 	  
		 	 }
		 	 
		 }
	}
	else
	{
		 if(dif>0)
		 	 return true;
		 else
		 	  return false;
	}
   return false;
}