/**
 * Copyright: Yao Yao
 */
 
#include "Gene.h" 
#include "Robot.h"
using namespace std; 
Gene::Gene()
{
} 

Gene:: Gene(int ID,int GfID,int Gene2ID,  int* Gs ,Rule * Ra  ,int sequenceL)
{ 
	

 emptygene  =false;

   
 
  ProductConcen=0;
  ProBind  = 0;
   
  shareNum=1;
 lastNum=1;
 GeneLength  = sequenceL ;  
GeneID=ID; 
 GenecID  = Gene2ID;
  
 product = new int [5];
  
for(int i=0;i<5; i++)
{
  product[i]=-1;
}
 
 GenefamilyID=GfID;
  RuleA  =Ra;
  
   
   Gsequence = new int [MaxGeneLength];
for(int i=0;i<GeneLength; i++) 
{
 Gsequence[i] = Gs [i];
}
 countArray=new int* [8];
for(int i=0;i<8; i++)
{
countArray[i]=new int[5];
}    

 Active= new int[20]; 

   Regulation  =new int * [50];   
  for(int i=0;i<50; i++) 
{
Regulation [i]=new int[3];
 Regulation [i][0] = -1;
 Regulation [i][1] = -6;
 Regulation [i][2] = -1;
}


  CMu();     
} 

void Gene::CMu()
{


int st;  int hun; int  ten; int inc;
 st = 7; 
  Cis_num  = Gsequence[4]+Gsequence[5]*4+  Gsequence[6]*4*4;  //0-40
   Cis_num  = Cis_num%40;
   
 Cis_num=Cis_num+10;

for(int i=0;i  <(Cis_num); i++) 
{
hun  = Gsequence[st]  + Gsequence[st+1]*4; ten  = Gsequence[st+2]  + Gsequence[st+3]*4;
 inc  = Gsequence[st+4]  + Gsequence[st+5]*4;

 hun=hun%10;  ten  = ten%10;  inc=inc%10;  
Regulation [i][0]=hun*100+ten*10+inc;
  inc = Gsequence[st+6]  + Gsequence[st+7]*4;  inc=inc%10;  Regulation [i][1]=inc;  // this is bind time
   inc = Gsequence[st+8]  + Gsequence[st+9]*4;  inc=inc%5;  Regulation [i][2]= inc;  // this is bind   rang  
 st=st+10;
}  
  
 ProduceRate  = Gsequence[st]  + Gsequence[st+1]*4;
 st++;
 st++;
  
  int tem;  
         tem=Gsequence[st];
if(Gsequence [st]==3)
Protein  =true;
else
Protein  =false;

st++;

if(Protein)
{
Pro_num  = Gsequence[st]+1;   // 1-4
st=st+1;
}
else
{
Pro_num=1;
}

for(int i=0;i  <  Pro_num; i++) 
{
hun  = Gsequence[st]  + Gsequence[st+1]*4; ten  = Gsequence[st+2]  + Gsequence[st+3]*4;
 inc  = Gsequence[st+4]  + Gsequence[st+5]*4; 
  
  
   
  
         hun=hun%10;  ten  = ten%10;  inc=inc%10; 
 product[i]=hun*100+ten*10+inc;
 st=st+6;
} 

if(tem==2 && product[0]>500)
{   RNAm=true;
product[0] =1007;
}
else
RNAm=false;
 
   mutation=Gsequence[st]+Gsequence[st+1]*4+  Gsequence[st+2]*4*4+Gsequence[st+3]*64;  mutation =mutation%100;   
}


   void Gene::ConnectR(  Robot * ta1  )
{
targetR=ta1;
}
void Gene::DeleteG()
{   
        GenefamilyID=-1; 
    GeneLength=0  ; 
  GenecID=-1;
	  shareNum =0;  emptygene=true;
  lastNum  = 0;
 //int avaNum;  int avaNum2; 
  mutation  = 0;



  
   }

void Gene::CheckGene(  int g,   int seq )
{//int TF_num  = targetR->environment[g][0];






int motif;  int concen;

srand((unsigned)time(NULL)+rand());
for(int n=0; n<1000; n++)
{
   motif=n;


 concen  = targetR->environment[g][n];
if(concen  > 0)
{
for(int t=0;t<Cis_num;t++)
{
if(targetR-> Cregulation   [seq][t][0] ==0&&Regulation[t][0]!=-1)
{
  motif=motif -Regulation[t][0];
//concen  =  targetR->environment[g][n];
if(motif<0)
  motif  = motif*-1;

if(concen  > 0 &&  motif<=5)
{

  int ra1  = 100 -  (motif*20);
  int thr=0;

for(int c=0;c<concen;  c++)
{ 
 thr=rand()%100;

 if(  motif<=Regulation [t][2]&&thr<=  ra1&&Regulation [t][2]>=0)
{
  targetR-> Cregulation   [seq][t][0]=1;
          targetR-> Cregulation   [seq][t][1]  = Regulation  [t][1] ;
   targetR->environment[g][n]   = targetR->environment[g][n]-1;
  c= concen+90;
}
}
}
}
if(  targetR->environment[g][n]<=0)
 t=Cis_num+90;
}
} 

}


}

 int Gene::Expression  (int& bindings, int seq)
{
for(int i =0; i <20;i++)
{  
  Active[i]  = 0;
} 
double percent;
srand((unsigned)time(NULL)+rand());
int index1;
 
 for(int t=0;t<Cis_num;t++)
{
if( targetR-> Cregulation   [seq][t][0] ==1)
{
if(Regulation[t][0]>=500)
{
index1=0;
}
else
{
index1=4;
}
  int type;
 type=Regulation[t][0]%10;
  
if(index1==0)
 Active[type]++;
if(index1==4)
 Active[type+10]++; 
switch(type)
{
case 0:
 countArray[index1][0]++;
break;
case 1:
countArray[index1+1][0]++;
break;
case 2:
countArray[index1+3][0]++;
break;
case 3:
countArray[index1+2][0]++;
break;
case 4:
countArray[index1+3][1]++;
break;
case 5:
countArray[index1][1]++;
break;
case 6:
countArray[index1+3][2]++;
countArray[index1+2][1]++;
break;
case 7:
countArray[index1+1][1]++;
break;
case 8:
countArray[index1+3][3]++;
break;
case 9:
countArray[index1+2][2]++;
break;		
default:
break;
}
if(targetR->Cregulation   [seq][t][1]>0)
targetR-> Cregulation   [seq][t][1]--;
if( targetR-> Cregulation   [seq][t][1]<=0)
 targetR-> Cregulation   [seq][t][0]  = 0;
}
}
// pick the smallest value

if(countArray[0][0]<=countArray[0][1])
countArray[0][4] =countArray[0][0];
else
countArray[0][4] =countArray[0][1];
if(countArray[1][0]<=countArray[1][1])
countArray[1][4] =countArray[1][0];
else
countArray[1][4] =countArray[1][1];
if(countArray[4][0]<=countArray[4][1])
countArray[4][4] =countArray[4][0];
else
countArray[4][4] =countArray[4][1];
if(countArray[5][0]<=countArray[5][1])
countArray[5][4] =countArray[5][0];
else
countArray[5][4] =countArray[5][1];
  
int small;
small=countArray[2][0];
if(small  > countArray[2][1])
small =countArray[2][1];
if(small  > countArray[2][2])
small =countArray[2][2];
countArray[2][4]=small;

small=countArray[6][0];
if(small  > countArray[6][1])
small =countArray[6][1];
if(small  > countArray[6][2])
small =countArray[6][2];
countArray[6][4]=small;

small=countArray[3][0];
if(small  > countArray[3][1])
small =countArray[3][1];
if(small  > countArray[3][2])
small =countArray[3][2];
if(small  > countArray[3][3])
small =countArray[3][3];
countArray[3][4]=small;

small=countArray[7][0];
if(small  > countArray[7][1])
small =countArray[7][1];
if(small  > countArray[7][2])
small =countArray[7][2];
if(small  > countArray[7][3])
small =countArray[7][3];
countArray[7][4]=small;


//  1 represent default   minimun expressio
int present  = 1; 
double inf;
for(int i=0;i<8; i++)
{
if(countArray[i][4]>=5)  
countArray[i][4]=5;
if(countArray[i][4]>=1)
{
if(i<=1)
inf=1.1;
if(i==2)
inf=1.3;
if(i==3)
inf=1.8;
if(i==4||i==5)
inf=0.9;
if(i==6)
inf= 0.7;
if(i==7)
inf=0;
for(int m=0;m<countArray[i][4];m++)
{
  present  = present*inf;
}

}

}

for(int i=0;i<8; i++)
{
countArray[i][0]=0;countArray[i][1]=0;countArray[i][2]=0;countArray[i][3]=0;//countArray[i][4]=0;
}
  ProBind  =0;
 if(present<1)
{
  ProductConcen  =0;
return 0;
}
else
{
 double  results;
  results= 1000 * (present-1);
  int bindpro;  
  bindpro  = (int)results;
  if(bindpro>1000)
 bindpro  =1000;
 for(int i=0;i<10; i++)
{
if(  RNAm )
{

 ProBind++;
if(i==9)
{
ProductConcen  = ProBind*ProduceRate; 
  return  3;
}
}
else
{
  
 // bindings is the number of RNA polymerase  
if(bindings>0)
{
int random=rand()%  1000;
  bindpro =  bindpro + 28 ;
if(bindpro>random)
{


  targetR->   genehis[seq][0]++;
 ProBind++;
  bindings--;
}
}
}
}

   
 ProductConcen  = ProBind*ProduceRate;
if( Protein)
return 2;
else
return 1;
}

}

  void Gene::CheckActivation( vector<double> * ActiveRate)
   {
  double totalAct= 10*countArray[0][4]+10*countArray[1][4]+ 30*countArray[2][4]+ 80*countArray[3][4]+10*countArray[4][4]+10*countArray[5][4]+ 30*countArray[6][4]+ 80*countArray[7][4]; 

 for(int i=0;i<20;  i++)
{

(*ActiveRate)[i]  =0;
}  

if(totalAct>0)
{ 
 if(countArray[0][4]>0)
 {
 (*ActiveRate)[0]  = 5*countArray[0][4];
 (*ActiveRate)[5]  = 5*countArray[0][4];
  }
   if(countArray[1][4]>0)
 {
 (*ActiveRate)[1]  = 5*countArray[1][4];
 (*ActiveRate)[7]  = 5*countArray[1][4];
  }
   if(countArray[2][4]>0)
 {
  (*ActiveRate)[3]  = 10*countArray[2][4];
  (*ActiveRate)[6]  = 10*countArray[2][4];
  (*ActiveRate)[9]  = 10*countArray[2][4];
  }
   if(countArray[3][4]>0)
 {
 
 
 (*ActiveRate)[2]  = 20*countArray[3][4];
 (*ActiveRate)[4]  = 20*countArray[3][4];
 (*ActiveRate)[6]  = 20*countArray[3][4] + (*ActiveRate)[6] ;
 (*ActiveRate)[8]  = 20*countArray[3][4];
  }
   if(countArray[4][4]>0)
 {
  (*ActiveRate)[10]  = 5*countArray[4][4];
  (*ActiveRate)[15]  = 5*countArray[4][4];
  }
   if(countArray[5][4]>0)
 {
  (*ActiveRate)[11]  = 5*countArray[5][4];
  (*ActiveRate)[17]  = 5*countArray[5][4];  
  }
   if(countArray[6][4]>0)
 {
  (*ActiveRate)[13]  = 10*countArray[6][4];
  (*ActiveRate)[16]  = 10*countArray[6][4];
  (*ActiveRate)[19]  = 10*countArray[6][4];  
  }
   if(countArray[7][4]>0)
 {
 (*ActiveRate)[12]  = 20*countArray[7][4];
 (*ActiveRate)[14]  = 20*countArray[7][4];
 (*ActiveRate)[16]  = 20*countArray[7][4]+(*ActiveRate)[16] ;
 (*ActiveRate)[18]  = 20*countArray[7][4];
  }



for(int i=0;i<20;  i++)
{

(*ActiveRate)[i]=(*ActiveRate)[i]/totalAct;
 if(Active[i]>0)
  (*ActiveRate)[i]  = (*ActiveRate)[i]/Active[i];
}

}





  }

 int  Gene::Mut(int pos,int cxe)
{

int tck  [GeneLength];

for(int i=0;i<GeneLength; i++)
{ 
tck[i]=Gsequence[i];
}

tck[pos]=cxe;






int nl =(tck[4]+tck[5]*4+tck[6]*4*4)%40;
nl=(nl+10)*10; 




if(tck[nl+5]==3)
      nl=tck[nl+6]*6+nl+1;

	  
  nl =nl+16; return nl;

}




 
 
  void Gene::ReadGenes(char* file, int x) 
        { 
                fstream Genestream; 
                Genestream.open(file, ios::in); 
    
                if(!Genestream.is_open()){ 
                        cerr<<"error file"<<endl; 
                        //return -1; 
                } 


                bool gc=true; 

		int j=0;    
                int i=0;  
				
                 
				while(gc){ 			
char* str=new char[77]; 
                    if(Genestream.getline(str, 72)) 
                        { 
 
                        if(Genestream.peek()==EOF) 
                        { 
                                gc=false;                        
         
                        } 

					}
                        if(j<x) 
                        j++; 
                        else 
                        { 
                                gc=false; 
                        } delete [] str;
 
				} 


				Genestream.close();  //     ????????????????????????

//      return Garray; 
        } 
  

  
  
  
 /*
  
  void Gene::WriteG(char* file,  int n ) //  there are two types  create new and write   write back       before use this function have to use updategene to update the length
        { 

   stringstream ss;
       ss  << n; 
	    string file1  = file+"/gene" +ss.str()  +".txt";             
                 
                ofstream out(file1.c_str(),ios::trunc); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 

               for(int z=0;  z<GeneLength ;z++)
{
              out<<Gsequence[z];
}
  out<<endl;
  out<<"Length "<<GeneLength;  out<<endl;
  out<<"Gene ID "<<GeneID;		   

 out.close(); 
        } 

		
		
		*/

 void Gene::PrintoutGene(string &Geneseq)
{
	stringstream n;




Geneseq="";	
for(int i=0;i<GeneLength; i++) 
{
  n<<  Gsequence[i];	
  
} 
Geneseq  = Geneseq+ n.str();
}



int Gene::CheckIncre()
{
int distance  = 0;
distance  = shareNum-lastNum;

   lastNum=shareNum;
  return distance;
}