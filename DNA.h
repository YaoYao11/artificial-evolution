/*
 * DNA.h
 *
 *  Created on: 2013/12/17 
 *      Author: Yao Yao
 */

#ifndef DNA_H_
#define DNA_H_
#include <time.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <iostream> 
#include <cmath> 
#include <iomanip> 
#include<string> 
#include <vector>
#include <fstream> 
#include <math.h> 
#include "Rule.h"
#define MaxDNALength 6000

 
class DNA{ 
public: 
DNA() ;
DNA (int ID,int  DfID,int DNA2ID,int DLength,int* dsequence,Rule* Ru);  
int SubCheck( int * g, int q);
void Update  (int ID,int  DfID,int DNA2ID,Rule* Ru,int * sequence,  int length);
void DeleteDNA();
void PrintoutDNA(string &DNAseq);
Rule * RuleB;		
int DNAID;  //DNA name
int DNAfamilyID;
int DNALength;  int DNAcID;  //DNA position number on vector
int* Nsequence;
vector< vector<int> >  subtyp;   // subtype ID, from which subtype, position, changed,  how much alive carre
int shareNum;  //how many dna in population
  };

  
#endif /* DNA_H_ */