/*
 * Event.h
 *
 *  Created on: 2014/02/11 
 *      Author: Yao Yao
 */
#ifndef Event_H_
#define Event_H_
#include <time.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <cmath> 
#include <iomanip> 
#include<string> 
#include <fstream> 
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <math.h>
#include <sys/time.h>
#include <cstdlib>
#include "Robot.h"
#include "Gene.h"
#include "Rule.h"


 
 
class Event{ 
public:
 Event()  ; 
 Event (Robot ** R1,  Gene ** Ge0,Rule* Ru1,int de, int * condit,  int cum  , int eng, double decay);
 bool Update  (double m);
 bool CollectData  (int r1);
 void record(int c);
 void   Renew (int de, int * condit,  int cum  , int eng, double decay) ;	 
 void     DeleteEvent  ();
 inline bool CheckMR(int x);
 
 vector< bool >   results;
 vector< vector<int> > conditions;
 int Deter  ;
 bool empty;
 vector< int > Con;
 vector< int >  Robots;
 Rule* Rul;
 Robot ** Rlist;
 Gene ** Glist;
 int top; 
 int extop;
 int   dtop;	 
 int lifetime;
 double AvM;
 double credit;
 double   decayRate;
};  





  
 #endif /* Event_H_ */
  
  