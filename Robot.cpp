/**
 * Copyright: Yao Yao
 */
#include "Robot.h"
using namespace std; 
 Robot::Robot()
{
}

Robot::Robot(int x, int y,int z,int p1,int g1,int t1,  Rule * ruleAgent,  Mutation  * mut,int geneNum, int dnaNum,  int** DNAs,int ** Genes,DNA **  dna, Gene** gene,bool readD)
{
   
  GenomeLength  =geneNum; 
 readDNA=readD;
   Murate=0;
  DNALength = dnaNum;
ID   =p1;
   Mucount=0;
  CMucount  = 0;  
 HMucount =0; 
empty  = false;

  Duplicate =0;
xc=x;
 zc =z;
yc=y;
lifetime  =0;      Atime=t1;
     expressh  =  0;  
 M  = mut;
  interact=ruleAgent;  genes =gene; dnas  = dna;
Rep =false;
 energy  =10000;
 attacked =false; // attacked
  
 moved  = false;  
   cooprater  = false;   // cooperate  attack singnal only means  possibility  not decide
 coopraNum  =0;
 
  expressh  =  0; 
 attackv  = 0; lastout2  = -1;
 
    preyt=0;
 defencev = 0;
cordefen  = 0;
x1  = -1;
y1=-1; 
totalRNA  = 0;
SequenceLength  = g1;  
energyCost  = 0;
energyCostn  = 0;
  energyCostl  = 0;

  
output=new int [8]; 
sen=new double [12];

 for( int h=0; h <12; h++)           //
{
   sen[h]  =0;
}

  for( int h=0; h <8; h++)           //
{
   output[h]  =0;
}
 

//Cregulation(0);
//genome(0);
//DNAgen(0);

 for( int h=0; h <GenomeLength; h++)           //
{  
  vector< vector<int> > dat(0); 
    vector<double> datdou(20); 
  for(   int i=0;  i <50; i++)
{
   vector<int> dat2(10);   
   
   
  for(   int z=0; z<10; z++)
{
   dat2[z]=0;
}

dat.push_back(dat2);
}

  Cregulation.push_back(dat); 
 ActiveRate.push_back(datdou);
   }
   
for(int h=0; h <DNALength;h++)
{
 vector<int> dat1(4);
for(int i=0;i<3;i++)
{
   dat1[i]  = DNAs[h][i];   
}
dnas[DNAs[h][0]]->shareNum++;
dat1[3]=0;
DNAgen.push_back(dat1);
}


  for(int h=0; h  < GenomeLength;h++)
   {
     vote.push_back(0);
	  vote1.push_back (0);
   }

for(int h=0; h <GenomeLength;h++)
{
 vector<int> dat1(3); 
vector<int> dat2(2);
for(int i=0;i<3;i++)
{
   dat1[i]  = Genes[h][i];

   
}
   genes[Genes[h][0]]->shareNum++;
 genome.push_back(dat1); 
 
genehis.push_back(dat2);

}
 
 totalenv = new int[Mo_kind];
  environment  = new int*[10];
  
  
  for(int h=0;h<Mo_kind;h++)
	{	
	  totalenv[h]=0;
       } 
	   
	 for(int i=0; i  < 10  ; i++)
	{
 environment[i]=new int [Mo_kind];
for(int h=0;h<Mo_kind;h++)
	{	
	  environment [i][h]  = 0;
       }
  }
   generation =0;
   Complex=0;Complex2=0;Complex3=0;
}

Robot::Robot(int x, int y,int z,int p1 ,int t1,  Rule * ruleAgent,  Mutation  * mut ,vector< vector<int> > * DNAs,vector< vector<int> > * Genes,DNA **  dna, Gene** gene,bool readD, Robot * tar)
{

  GenomeLength  = tar->GenomeLength; 
 readDNA=tar->readDNA;
  DNALength = tar->DNALength;
ID   =p1;


  Murate=0;
 Mucount =0;
  CMucount =0; HMucount =tar->HMucount;
empty  = false;
xc=x;
yc=y;
 zc = z; Duplicate  = tar->Duplicate;
lifetime  =0;      Atime=t1;expressh  =  0; 
 M  = mut;
  interact=ruleAgent;  genes =gene; dnas  = dna;
Rep =false;
 energy=tar->energy/2;  
   tar->energy=energy;
 attacked =false; // attacked 
         moved  = false;
   cooprater  = false;   // cooperate  attack singnal only means  possibility  not decide
 coopraNum  =0;
 attackv  = 0; 
  lastout2  = -1;
 
    preyt=0;
 defencev = 0;
cordefen  = 0;
x1  = -1;
y1=-1; 
totalRNA  = 0;
SequenceLength= tar->SequenceLength;  

 
   energyCost  = 0;
energyCostn  = 0;
  energyCostl  = 0;
  
output=new int [8]; 
sen=new double [12];

 for( int h=0; h <12; h++)           //
{
   sen[h]  =0;
}

  for( int h=0; h <8; h++)           //
{
   output[h]  =0;
}

 //cerr<<" GenomeLength "<<  GenomeLength<<endl; 

        //Cregulation(0);
//genome(0);
//DNAgen(0);

 for( int h=0; h <GenomeLength; h++)           //
{  
 
  vector< vector<int> > dat(0);
 //cerr<<"  h1 "<<  h<<endl; 
    vector<double> datdou(20); 
  for(   int i=0;  i <50; i++)
{
   vector<int> dat2(10);   
   
   
  for(   int z=0; z<10; z++)
{
   dat2[z]=0;
}

dat.push_back(dat2);
}

  Cregulation.push_back(dat); 
 ActiveRate.push_back(datdou);
   }
//cerr<<" DNALength "<<  DNALength<<endl; 
for(int h=0; h <DNALength;h++)
{
 vector<int> dat1(4);
for(int i=0;i<3;i++)
{
   dat1[i]  = (*DNAs)[h][i];   
}
dnas[(*DNAs)[h][0]]->shareNum++;
dat1[3]=0;
DNAgen.push_back(dat1);
}



for(int h=0; h <GenomeLength;h++)
{
 vector<int> dat1(3); 
vector<int> dat2(2);
for(int i=0;i<3;i++)
{
   dat1[i]  = (*Genes)[h][i];

   
}
   genes[(*Genes)[h][0]]->shareNum++;
 genome.push_back(dat1); 
 
genehis.push_back(dat2);

}


   for(int h=0; h  < GenomeLength;h++)
   {
     vote.push_back(0);
	  vote1.push_back (0);
   }


//cerr<<" Mo_kind "<<  Mo_kind<<endl;
 totalenv = new int[Mo_kind];
  environment  = new int*[10];
  
  
  for(int h=0;h<Mo_kind;h++)
	{	
 
  tar->totalenv[h]=tar->totalenv[h]/2;	
	  totalenv[h]= tar->totalenv[h];
       } 
	   
	 for(int i=0; i  < 10  ; i++)
	{
 environment[i]=new int [Mo_kind];
for(int h=0;h<Mo_kind;h++)
  {
   tar->environment[i][h]=tar->environment[i][h]/2;
	  environment [i][h]  =tar->environment[i][h];
       }
  }
generation =0;
Complex=0; Complex2=0;  Complex3=0;
}

void Robot::EWGD  (Robot * rob)
{	
   cerr<<"  sexual duplication "<<endl;
  int Gl2=rob->GenomeLength;
  int DN2  =rob->DNALength;
  

 for( int h=GenomeLength; h <(GenomeLength+Gl2); h++)           //
{  
  vector< vector<int> > dat(0); 
    vector<double> datdou(20); 
  for(   int i=0;  i <50; i++)
{
   vector<int> dat2(10);   
   
   
  for(   int z=0; z<10; z++)
{
   dat2[z]=0;
}

dat.push_back(dat2);
}

Cregulation.push_back(dat); 
ActiveRate.push_back(datdou);
   }
   
  
   for( int h=GenomeLength;h <(GenomeLength+Gl2);h++)
   {
   	vote.push_back(0);
	vote1.push_back (0);
   }
   
  //cerr<<" c   -3 "<<endl; 
for(int h=DNALength; h<(DNALength+DN2);h++)
{
 vector<int> dat1(4);
for(int i=0;i<3;i++)
{
   dat1[i]  =rob->DNAgen[h-DNALength][i];   
}
dat1[3]=0;
DNAgen.push_back(dat1);

dnas[rob->DNAgen[h-DNALength][0]]->shareNum++;
}
 //cerr<<" c   -4 "<<endl; 


  for( int h=GenomeLength; h <(GenomeLength+Gl2);h++)
{
 vector<int> dat1(3); 
vector<int> dat2(2);
for(int i=0;i<3;i++)
{
   dat1[i] =rob->genome[h-GenomeLength][i];

   
}
   genome.push_back(dat1); 
 
   genehis.push_back(dat2);
 genes[rob->genome[h-GenomeLength][0]]->shareNum++;   
   

}
 
 GenomeLength=GenomeLength+Gl2; 
 DNALength=DNALength+DN2;
 Duplicate=Duplicate + rob->Duplicate+1;
 SequenceLength  = SequenceLength+rob->SequenceLength;
  Mucount = Mucount+rob->Mucount;
 CMucount =CMucount+rob->CMucount;
   HMucount=HMucount+rob->HMucount;
        cerr<<"  sexual duplication finish "<<endl;
}

void Robot::WGD()
{	

 //cerr<<" c   -2 "<<endl;

 
 

 for( int h=GenomeLength; h <GenomeLength*2; h++)           //
{  
  vector< vector<int> > dat(0); 
    vector<double> datdou(20); 
  for(   int i=0;  i <50; i++)
{
   vector<int> dat2(10);   
   
   
  for(   int z=0; z<10; z++)
{
   dat2[z]=0;
}

dat.push_back(dat2);
}

Cregulation.push_back(dat); 
ActiveRate.push_back(datdou);
   }
   
  
   for( int h=GenomeLength; h <GenomeLength*2;h++)
   {
   	vote.push_back(0);
	vote1.push_back (0);
   }
   
  //cerr<<" c   -3 "<<endl; 
for(int h=DNALength; h<DNALength*2;h++)
{
 vector<int> dat1(4);
for(int i=0;i<3;i++)
{
   dat1[i]  =DNAgen[h-DNALength][i];   
}
dat1[3]=0;
DNAgen.push_back(dat1);

dnas[DNAgen[h-DNALength][0]]->shareNum++;
}
 //cerr<<" c   -4 "<<endl; 


  for( int h=GenomeLength; h <GenomeLength*2;h++)
{
 vector<int> dat1(3); 
vector<int> dat2(2);
for(int i=0;i<3;i++)
{
   dat1[i]  = genome[h-GenomeLength][i];

   
}
   genome.push_back(dat1); 
 
   genehis.push_back(dat2);
 genes[genome[h-GenomeLength][0]]->shareNum++;   
   

}
 
 GenomeLength=GenomeLength*2; 
 DNALength=DNALength*2;
 Duplicate++;
 SequenceLength  = SequenceLength*2;
  Mucount = Mucount*2;
 CMucount =CMucount*2;
   HMucount=HMucount*2; 

}


void Robot::sense(double * input)
{
//
if(ID==8)
cerr<<" Complex-- "<< Complex3 << " c1 "<< Complex << " c2 " << Complex2 <<endl;
Complex=0;Complex2=0;
for(int i=0; i<4;i++)
{
//cerr<<" sense 0 "<< input[i]<< " "<<i<<endl; 
sen[i] =input[i];
//cerr<<" sense 1 "<< input[i]<<endl; 
}
      sen[2]=sen[2]/10000;  
	
  for( int h=0; h <8; h++)           //
{
   output[h]  =output[h]/3;
}
	
        
   int con1=2;
  sen[4]  = energyCostn-energyCostl;  //      +-100  energy cost  distance before
    sen[4]  =25+(sen[4]/4);
 
   if(sen[4]<0)
     sen[4]=  0;

   sen[6] =  energy/10000.0*100.0;
	 
 sen [7] =sen[7]  + attackt*10 +preyt*20; 
  
  if(sen[7]>5)
    sen[7]=sen[7]-5;
else
{	
   if(sen[7]<-5)
    sen[7]=sen[7]+5;
	 else
	  sen[7]=0;
	}
 if(sen [7]>100)
   sen [7] =100;
   if(sen [7]<-100)
      sen [7] =-100;
   

 sen [8] =sen[8]+defendt*10; 
  if(sen[8]>5)
    sen[8]=sen[8]-5;
else
{	
   if(sen[8]<-5)
    sen[8]=sen[8]+5;
	 else
	  sen[8]=0;
	}
 if(sen [8]>100)
   sen[8]=100;
   if(sen[8]<-100)
      sen[8]=-100;

 //cerr<<" 3-10 "<<endl;
	  
	  
  if(energy >0 )
{
  sen[9]  =attackv/(energy*1.0);  sen[10]  =defencev/(energy*1.0); 
}
else
{
sen[9]  =0;  sen[10]  =0; 
}   
   
      //cerr<<" 3-11 "<<endl;
 sen[11]++;
	  
int svalue;
svalue =0;
svalue = sen[0]*5;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [0]] =totalenv[interact->signal [0]] +svalue*con1;
totalenv[interact->signal [1]] =totalenv[interact->signal [1]] +svalue*con1;
svalue = sen[1]*10;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [2]] =totalenv[interact->signal [2]] +svalue*con1;
totalenv[interact->signal [3]] =totalenv[interact->signal [3]] +svalue*con1;
svalue = sen[2]*5;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [4]] =totalenv[interact->signal [4]] +svalue*con1;
totalenv[interact->signal [5]] =totalenv[interact->signal [5]] +svalue*con1;
svalue = sen[3]*10;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [6]] =totalenv[interact->signal [6]] +svalue*con1;
totalenv[interact->signal [7]] =totalenv[interact->signal [7]] +svalue*con1;


svalue = sen[4];
if(svalue>50)
     svalue=50;
totalenv[interact->signal [8]] =totalenv[interact->signal [8]] +svalue*con1;
totalenv[interact->signal [9]] =totalenv[interact->signal [9]] +svalue*con1;
 //cerr<<" 3-12 "<<endl;
svalue =  TotalE/400;  // assume the max earn energy is  20000, the robot is 25000
svalue =  svalue+fightEarn/800;  
 svalue =  svalue +preyE/400;
 //svalue = sen[5]/400;
svalue=svalue  -5;
if(svalue>50)
     svalue=50;
if(svalue  < 0 )
     svalue=0;
totalenv[interact->signal [10]] =totalenv[interact->signal [10]] +svalue*con1;
totalenv[interact->signal [11]] =totalenv[interact->signal [11]] +svalue*con1;


 //cerr<<" 3-15 "<<endl;

if(sen[6]>=200)
{
totalenv[interact->signal [12]] =totalenv[interact->signal [12]] +10*con1;
totalenv[interact->signal [13]] =totalenv[interact->signal [13]] +10*con1;
}

if(sen[6]>=150)
{
totalenv[interact->signal [12]] =totalenv[interact->signal [12]] +10*con1;
totalenv[interact->signal [13]] =totalenv[interact->signal [13]] +10*con1;}

if(sen[6]>=100)
{
totalenv[interact->signal [12]] =totalenv[interact->signal [12]] +10*con1;
totalenv[interact->signal [13]] =totalenv[interact->signal [13]] +10*con1;
}

if(sen[6]>=50)
{
totalenv[interact->signal [12]] =totalenv[interact->signal [12]] +10*con1;
totalenv[interact->signal [13]] =totalenv[interact->signal [13]] +10*con1;
}
if(sen[6]>=25)
{
totalenv[interact->signal [12]] =totalenv[interact->signal [12]] +10*con1;
totalenv[interact->signal [13]] =totalenv[interact->signal [13]] +10*con1;
}
 //cerr<<" 3-16 "<<endl;

 
 

 
svalue=25+(sen[7]/4);

totalenv[interact->signal [14]] =totalenv[interact->signal [14]]  + svalue*con1;
totalenv[interact->signal [15]] =totalenv[interact->signal [15]]  + svalue*con1;

svalue=25+(sen[8]/4);
totalenv[interact->signal [16]] =totalenv[interact->signal [16]]  + svalue*con1;
totalenv[interact->signal [17]] =totalenv[interact->signal [17]]  + svalue*con1;




svalue = sen[9]  * 20;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [18]] =totalenv[interact->signal [18]] +svalue*con1;
totalenv[interact->signal [19]] =totalenv[interact->signal [19]] +svalue*con1;

svalue = sen[10]  * 20;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [20]] =totalenv[interact->signal [20]] +svalue*con1;
totalenv[interact->signal [21]] =totalenv[interact->signal [21]] +svalue*con1;

svalue = sen[11]/2;
if(svalue>50)
     svalue=50;
totalenv[interact->signal [22]]  = svalue*con1;
totalenv[interact->signal [23]] = svalue*con1;  
 
 
  //cerr<<"   821 "<<endl;


  
	  }


bool Robot::Reproduce ()
{
if(!Rep)
 return  false;
if(energy  > 15000)
{

int distance;
distance = energy  - 15000;
int rn;
srand((unsigned)time(NULL)+rand());
 rn  = rand()%10000;
 if(distance  >=rn)
   return true;
  else
  return false; 
}
else
return false;
}

int Robot::CreatNewRobot(Robot * tar, int z) 
{
 //cerr<<" c-1 "<<endl;
      tar->GenomeLength  =GenomeLength; 
 tar->readDNA= readDNA;
  tar->DNALength = DNALength;


//empty  =false;
tar->xc=xc;
tar->yc=yc; 
  tar->zc =z;
tar->lifetime  =0;      tar->Atime=Atime; tar->expressh  =  0;  
 tar->M  = M;
 
      tar->Murate=0;
 tar->Duplicate  =  Duplicate;
   tar->Mucount  =0;
  tar->CMucount=0;  
 tar->HMucount=HMucount;
  tar->interact  = interact;  
 tar->genes =genes; tar->dnas  = dnas;
 tar->Rep =false;      
   energy =energy/2; 
  tar->energy =energy; 
 tar->attacked =false; // attacked 
   tar->moved  = false;
   tar->cooprater  = false;   // cooperate  attack singnal only means  possibility  not decide
 tar->coopraNum  =0;
 tar->attackv  = 0; 
 tar->lastout2  = -1;
tar->preyt=0;
tar->defencev = 0;
tar->cordefen  = 0;
tar->x1  = -1;
tar->y1=-1;
 tar->totalRNA  = 0;
tar->SequenceLength  = SequenceLength;  
tar->energyCost  = 0; 
 tar->energyCostn  = 0;
  
         tar->energyCostl  = 0;
 //cerr<<" c   -2 "<<endl;

 
 

//Cregulation(0);
//genome(0);
//DNAgen(0);

 for( int h=0; h <GenomeLength; h++)           //
{  
  vector< vector<int> > dat(0); 
    vector<double> datdou(20); 
  for(   int i=0;  i <50; i++)
{
   vector<int> dat2(10);   
   
   
  for(   int z=0; z<10; z++)
{
   dat2[z]=0;
}

dat.push_back(dat2);
}




  tar->Cregulation.push_back(dat); 
 tar->ActiveRate.push_back(datdou);
   }
   
  
         for(int h=0; h  < GenomeLength;h++)
   {
     tar->vote.push_back(0);
	  tar->vote1.push_back (0);
   }
   
  //cerr<<" c   -3 "<<endl; 
for(int h=0; h <DNALength;h++)
{
 vector<int> dat1(4);
for(int i=0;i<3;i++)
{
   dat1[i]  =DNAgen[h][i];   
}
dat1[3]=0;
 dnas[DNAgen[h][0]]->shareNum++;
  tar->DNAgen.push_back(dat1);
}
 //cerr<<" c   -4 "<<endl; 


for(int h=0; h <GenomeLength;h++)
{
 vector<int> dat1(3); 
vector<int> dat2(2);
for(int i=0;i<3;i++)
{
   dat1[i]  = genome[h][i];

   
}
   genes[genome[h][0]]->shareNum++;
 tar->genome.push_back(dat1); 
 
tar->genehis.push_back(dat2);

}
 
 //cerr<<" c  -5 "<<endl; 
  
  
  for(int h=0;h<Mo_kind;h++)
	{
         totalenv[h]= totalenv[h]/2;	
	  tar->totalenv[h]=totalenv[h];
       } 
	   
	 for(int i=0; i  < 10  ; i++)
	{
for(int h=0;h<Mo_kind;h++)
	{	
	  environment[i][h]=environment[i][h]/2; 
	   tar->environment[i][h]=environment[i][h];
       }
  }
   //cerr<<" c  -6 "<<endl;  
 tar->generation  = generation+1;
tar->Complex=0;

tar->Complex2=0;
tar->Complex3=0;
tar->readDNA=readDNA;

tar->GenomeLength  = M->Havemutation(&(tar->genome),&(tar->DNAgen),&(tar->Cregulation), &(tar->ActiveRate), tar->readDNA ,  tar->ID );
 tar-> DNALength=tar->DNAgen.size();
  //cerr<<"  empty"<<endl;
   tar->empty=false;
 
  tar->lifetime=0;
     
   return tar->ID;
}

void   Robot::Checkup()
{
  if(SequenceLength!=0)
         Murate=Mucount/(SequenceLength/10000.0);
for(int h=0;h<Mo_kind;h++)
 {
  for(int i=0; i  < 10  ; i++)
 {
   if(environment[i][h]>1)
  {
     Complex2++;
  } 
 } 
 }		 
	Complex3 =(Complex/(GenomeLength/10.0))*(Complex2/(GenomeLength/10.0));
}
void Robot::GRNcircle  (int xr,int yr, int xe,int ye )
{
 int val1; int val2; 
  int val3;
// re-iniialize the robot every time

  //cerr<<"  grn "<<endl;
   

   Rep=false;
  coopraNum = 0; 
 
  lifetime++;
attacked  = false; 
moved  =false;
cordefen  =0;
x1 =-1;
y1 =-1;
 
//  
for(int h=0;h<Mo_kind;h++)
 
 {
   if(totalenv[h]>0)
  {
     totalenv[h]  = (int)(totalenv[h]-totalenv[h]*interact->decayRate[h]);
 }   
 }
   //cerr<<"  grn 2 "<<endl;
  srand((unsigned)time(NULL)+rand());     	
	for(int h=0;h<1000;h++)
	{
	 val1  = totalenv[h]/10;
	 val2  = totalenv[h]%10;
	 val3  = rand ()%10;
	 for(int i=0; i  < 10  ; i++)
	{
	  environment [i][h]  = val1;
	}
	  environment [val3][h] =environment [val3][h]+val2;
  }
   //cerr<<"  grn 3 "<<endl;
   val1  = totalRNA/10;
	 val2  = totalRNA%10;
	 val3  = rand ()%10;  
	 for(int i=0; i  < 10  ; i++)
	{
	     environment  [i][1007]= val1;
	}
	  environment[val3][1007]=environment[val3][1007]+val2;
 totalRNA=0;
     int ind;  
	int average;
	int check;
   average = SequenceLength/10;  ind =0;  check =0; 
     //cerr<<"  grn5 "<<endl;
 for(int h=0; h  < GenomeLength;h++)
{



 //cerr<<"  grn6 "  <<   GenomeLength   << " "  << h<<endl;
      //

genes[genome [h][0]]  ->ConnectR(  this);
 check =genome[h][1]+ genes[genome [h][0]]->GeneLength-1; 
  if(  check>average*(ind  +1 ))   
  { 

   if(ind<9)  
 ind++;
   // check=genome[h][2];     
  }

     //cerr <<genome [h][0]<<endl;
        
 genes [genome[h][0]]  -> CheckGene (ind , h); 
  //cerr<<"  grn7 "  << genome [h][0]<<endl;
   int ex;  

//cerr<<" genome[h][0] "<<genome[h][0]<<" proid "<< genes [genome[h][0]]->product[0] % 8<<"  oca "<<genes[genome[h][0]]->ProductConcen<<" ex "<<ex <<  " genomelength "<<GenomeLength<<endl; 
   ex = genes [genome[h][0]]  ->  Expression  (environment[ind][1007],h);
//cerr<<"  grn8 "  << endl;
     genes [genome[h][0]]  ->CheckActivation (&(ActiveRate[h])); 
//cerr<<"  grn9 "  << endl;	  
     genehis[h][2]=0;
   if(ex==3)
      totalRNA=totalRNA+genes[genome[h][0]]->ProductConcen;
   if(ex==1)
 totalenv[genes[genome[h][0]]->product[0]]  = totalenv[genes [genome[h][0]]->product[0]]+genes [genome[h][0]]->ProductConcen;
  if(ex ==2)
 {
for(int i =0; i <MaxProtein;i++)
{
 int proid  = genes [genome[h][0]]->product[i] %8;
        
   if(proid >=0)
      output[proid]=output[proid]+genes[genome[h][0]]->ProductConcen ;
	  else
	     i  =MaxProtein+1;
}
 genehis[h][2]=1; 
  }
 
 
  genehis[h][1]  = genes[genome[h][0]]->ProductConcen;
   //cerr<<" genehis[h][1] "<<genehis[h][1] <<endl;  
}



  //   use output decide status  one by one  
  
  
//if(ID==0&&!empty)
 //cerr<<" output "<<output[0]<<"   "<< output[1]<<"   "<<output[2]<<"   "<< output[3]<<"   "<<output[4]<<"   "<< output[5]<<"   "<<output[6]<<"   "<< output[7] <<endl;
if(lastout2==-1)
        lastout2=output[2]+79;
 energysave  = interact  ->DecideEngsave(output[2],lastout2);

lastout2  = output[2];  
if(output[0]>output[1])
{
x1=xr;  y1 =yr;
}
else
{
if(output[0]<output[1])
{
x1=xe;  y1 =ye;
}
}

if(output[3]>output[4])
{
 cooprater  =false;
  attacker =true; defender=false;
}
else
{
if(output[3]<output[4])
{
cooprater =true;  attacker =false;  defender=true;
}
if(output[3]==output[4])
{
int dice  = rand()%2;
if(dice==0)
cooprater  =false;
else
cooprater=true;
if(cooprater)
{
attacker =false;  defender=true;
}
else
{
attacker =true; defender=false;
}
}

}



int attenergy  = 0;
int defenergy  =0;  

output[7]= output[7]/2;  
output[6]= output[6]/2;
if  (energysave!=0)
{
defenergy= output[7]/energysave;  
attenergy= output[6]/energysave;
 }
 else
 {

  defenergy= output[7];  attenergy= output[6];	 
 }
 
 
 energy=energy-defenergy;  
 energy=energy-attenergy;
 attackv= attackv + attenergy*2;
 defencev=defencev +defenergy*2;
if(  interact->DecideReproduce (output[5],energy)&&energysave<5)
   Rep  =true;
else
 Rep =false;}



void Robot::PPI()
{
 
  srand((unsigned)time(NULL)+rand());
int ten2;  int ten; int single;   int single2; int tv;
 int minival;
  for(int i =0; i <1000; i++)
{
if(totalenv [i]>5)
{
ten=i/10;
single=i%10;  
 ten2=99-ten;
  single2  = (single+2) % 10;
  tv=ten2*10+single2;
if(totalenv [tv]>5)
{
if(totalenv [tv]>totalenv  [i])
minival=totalenv  [i];
else
minival=totalenv[tv];
minival=minival*  interact->PPIrate[single];
minival=minival/100;
if(minival>=1)
{
totalenv [tv]=totalenv [tv]-minival;  
totalenv [i]=totalenv [i]-minival;
  tv  =(ten-ten2)*10;
 if(tv<0)
  tv = -tv;
 single2 = (single+single2)  % 10;
tv = tv+single2;  
totalenv [tv]=totalenv [tv] +minival;
}
}
 single2  = (single + 3) % 10; 
 tv=ten2*10+single2;

 if(totalenv [tv]>5&&totalenv [i]>5)
{
if(totalenv [tv]>totalenv  [i])
minival=totalenv  [i];
else
minival=totalenv[tv];
minival=minival*interact->PPIrate[single  + 10 ];
minival=minival/100;
if(minival>=1)
{
totalenv [tv]=totalenv [tv]-minival;  
totalenv [i]=totalenv [i]-minival;
  tv  =(ten-ten2)*10;
 if(tv<0)
  tv = -tv;
 single2 = (single+single2)  % 10;
tv = tv+single2;  
totalenv [tv]=totalenv [tv] +minival;
}
}


}
}
for(int i =0; i <1000; i++)
{
   int decay;
   decay = (totalenv [i]* interact->decayRate[i])/100;
   totalenv [i]  = totalenv [i]  - decay;   
}

}


int Robot::Cooperate()
{
if(cooprater)
return   defencev;
}

void Robot::SetCooperate  (int totaldef,int cop)
{

  
   coopraNum  = cop;
if(totaldef>1&&cooprater)
  cordefen  = (totaldef-defencev)/100*30;
 }

  void Robot::Makemove(  int cost)
{
   xc=x1;
     yc=y1; 
  x1=-1;  y1=-1; 

 //if(ID==89)  
 //cerr<<"  mov "  <<endl;
  
  moved  =true;  
 energy=energy-cost;
  energyCost =energyCost+cost;
 
}
void Robot::SetZ(  int z)
{
 //if(ID==89)  
 //cerr<<"  mov "<<z<<endl;
 zc=z;
}

int Robot::Getx()
{
return x1;
}
int Robot::Gety()
{
return y1;
}

int Robot::Attack(Robot* def)
{
int at1  =-1;  
int randomv; 
int distance;
double   drate=0;
double   drate2=0;
 attacked=true;
//cerr<<"   pas "  <<endl;
int dv=def->defencev  + def->energy +  def->cordefen;
distance =attackv-dv;
  //cerr<<"   pas  ok "  << endl ;
if(distance>0&&def->defender)
{

 //cerr<<"   pas  0 "  << endl ;
if(dv!=0)
{
   drate  = distance/(dv*1.0);
    drate2 =distance/(dv*1.5);
   drate = drate*100;  
   drate2  =drate2*100;
}
else
{

drate=100; drate2=100;
}    
if(drate>0)
{

srand((unsigned)time(NULL)+rand());	
   for(int i =0; i <10; i++)
   {
   randomv=rand()%100;
   if(randomv<=drate)
    at1  = 3;
	if(randomv<=drate2 &&  distance>(dv*1.2) )
	 {
    at1  =  1;
	 drate=def->energy*0.5;
	   int pe= (int)  drate;
	   energy =energy+pe;
  def->DeRobot();
	    i  = 17;

	}
   }
}
}
else
at1  =0;

if(def->attacker)
{
 //cerr<<"   pas  1 "  << endl ;
int distance2;
int dv2=defencev  + energy;
distance2=def->attackv  -dv2  ; 
double   drate3=0;
double   drate4=0;
if(distance>0&&dv!=0)
{
   drate  = distance/(dv*1.0);
    drate2 =distance/(dv*1.5);
   drate = drate*100;  
   drate2  =drate2*100;
   }
  if(distance2>0&&dv2!=0)
{
   drate3  = distance2/(dv2*1.0);
    drate4 =distance2/(dv2*1.5);
   drate3=drate3*100;  
   drate4=drate4*100;
  }  
 
  
  //cerr<<"   pas          2 "  << endl ; 
//cerr<<"     def " << def->empty<<" " <<def->energy<<endl ; 
 srand((unsigned)time(NULL)+rand());
 bool fir=true;

for(int i =0; i <10; i++)
   {
if(distance>0)
{
if(dv==0)
{
drate=100; 
 drate2=100;
}
randomv=rand()%100;
   if(randomv<=drate&&fir)
   {
    at1  = 3;
 fir=false;
	}
	if(randomv<=drate2 &&  distance>(dv*1.2) )
	 {
      //cerr<<"   drate2 " << drate<<" " << drate2<<endl ; 
    at1  = 1;
	 drate=def->energy*0.5;
	   int pe= (int)  drate;
	   energy =energy+pe;
 def->energy=def->energy  - 150;energy = energy-150;
 
 def->DeRobot();
 
	   return at1;
	   

	}

   }
 //cerr<<"   pas      08 "  << endl ; 
  if(distance2>0)
{
if(dv2==0)
{
 drate3=100; 
  drate4=100;
}
randomv=rand()%100;
   if(randomv<=drate3&&fir)
   {
    at1  = 4;  fir  = false;}
	if(randomv<=drate4 &&  distance2>(dv2*1.2) )
	 {
    at1  = 2;
	 drate3  = energy*0.5;
	   int pe= (int)  drate3;
def->energy=def->energy+pe;
def->energy=def->energy  - 150;energy = energy-150;
 return at1;

	}

   }  
   }
def->energy =def->energy  - 150;   energy = energy-150;

 return at1;
   
   
   
   }


energy = energy-150;
return at1;
}


void Robot::DeRobot()
{
empty=true;
energy=0;Duplicate =0;  lifetime  =0;

expressh  =  0; 
 Murate  = 0;
   Mucount  =0;
     CMucount  =0;
  HMucount  =0; 
cordefen  = 0;
attackv =0; lastout2  = -1;
defencev =0;  //cerr<<"   DeRobot 1 "   <<  DNAgen.size() <<  " "<<  genome.size() <<  GenomeLength<< DNALength  <<endl ;  
 DNALength =  DNAgen.size();
for(int h=0; h <DNALength;h++)
{
//cerr<<"   DeRobot 2 " <<  endl; 
//cerr << DNAgen[h][0]<<endl ;
   M->deleteDNA(DNAgen[h][0]);

 //         cerr<<"   DeRobot 2 1 "  << endl ;
}

for(int h=0; h <GenomeLength;h++)
{
//cerr<<"   DeRobot 3 "  <<  endl;  
//cerr <<genome[h][0]  <<endl ;
   M->deleteGene(genome[h][0]);
//    cerr<<"   DeRobot 8 "  << endl ;

}


 vector< vector< vector<int> > >().swap(  Cregulation);
   vector< vector<int> >().swap(  genome);  
  vector< vector<int> >().swap(  genehis); 
 vector< vector<int> >().swap(  DNAgen);  
   vector< vector<double> >().swap(  ActiveRate);
  vector<double>().swap(vote);
 vector<int>().swap(vote1);
//Cregulation(0);
//genome(0);
//DNAgen(0);

 
      
}

int Robot::Interact()
{
if(empty)
return   2;
if(moved)
 return   2;
if (attacked)
 return   2;
if(cooprater)
return   0;
else
return  1;
}


     inline int Robot::CheckBind(int motif1,  int motif2, int con)
{
  motif1  = motif1-motif2;
   if(motif1<0)
        motif1  = motif1*-1;
  
   if(motif1==0)
     return  con;
   else
    {
      if(motif1<5)
            {

			   con = con*(20*(5-motif1));
			     con =con/100;
             return con; 
			
             }			
	}


	
	
	
	
	
	
          return 0;

}  


 void Robot::WriteGenome( int robotId, int o_gnum,  int o_dnum )
{  





       stringstream ss;
       ss<<      robotId;

       
       int gc=0; int dc=0;  bool creadDNA  = readDNA; bool end=false;
       string buf="";
	
   string file="result/RobotID" +ss.str()  +"_g.txt";

ofstream out(file.c_str(),ios::trunc);
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 

				
 	while(  ((gc<genome.size())||(dc<DNAgen.size()))&&!end )
	{
		buf="";
		
		if(creadDNA&&(dc<DNAgen.size()))
		{
			   //cerr<<"  gc "<<gc  <<"  dc "<<dc<<"  gec  "<<genome[gc][0]<< "  dec "<< DNAgen[dc][2]<<" size gene "<<  genome.size()  <<"  size dna "<<  DNAgen.size() <<endl;
			dnas[DNAgen[dc][0]]->PrintoutDNA(buf);
			if(DNAgen[dc][0]<=o_dnum)
			out<<buf<<"  DNA "<< DNAgen[dc][0] << " copy number: " <<  dnas[DNAgen[dc][0]]->shareNum <<endl;
                        else
                        out<<buf<<"  new DNA "<< DNAgen[dc][0] << " copy number: " <<  dnas[DNAgen[dc][0]]->shareNum <<endl;
			if(DNAgen[dc][2]==0)
				creadDNA=true;
			else
				creadDNA =false;
			
			
			   dc++;
			   if(dc>=DNAgen.size())
			   	   creadDNA =false;			   	   

			   
			
		}
		  else
	       {
	       	     if(!creadDNA&&(gc<genome.size()))
	       	     {
	       	         //cerr<<"  gc "<<gc  <<"  dc "<<dc<<"  gec  "<<genome[gc][0]<<endl;     
	                genes[genome[gc][0]]->PrintoutGene(buf);
	                if(genome[gc][0]<=o_gnum)
			out<<buf<<"  Gene "<<genome[gc][0]<<" copy number: "<<genes[genome[gc][0]]->shareNum<<endl;
		        else
		        out<<buf<<"  new_Gene "<<genome[gc][0]<<" copy number: "<<genes[genome[gc][0]]->shareNum<<endl;
			  
			if(genome[gc][2]==0)
				creadDNA=false;
			else
				creadDNA =true;
			gc++;
			
			
		     }
		     else
		     	     end  =true;
			
		}
  }	
    

  out<<" ID "<<ID;
 
out<<" WGD times: " <<   Duplicate << " generation: " <<generation;
 out.close();

}



 void Robot::Write2( int robotId  )
{  





       stringstream ss;
       ss<<      robotId;

     

	
   string file="result/RobotID" +ss.str()  +"_e.txt";

ofstream out(file.c_str(),ios::app); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 

				
 	for(int i=0; i  < 10  ; i++)
	{

for(int h=0;h<Mo_kind;h++)
	{out<<" "<<environment [i][h];

       }
	  out<< endl;	   
  }	
 out<<" time step "<< Atime;out<< endl;	
 out.close();

}

void Robot::Write3( int robotId  )
{  





       stringstream ss;
       ss<<      robotId;

     

	
   string file="result/RobotID" +ss.str()  +"_t.txt";

ofstream out(file.c_str(),ios::app); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 

				
 	for(int i=0; i  <GenomeLength ; i++)
	{
out<< " "<< genes[genome[i][0]]->ProductConcen ;


  }	
 out<<endl;

 
 out.close();

}

 void Robot::Write1( int robotId  )
{  





       stringstream ss;
       ss<<      robotId;

     

	
   string file="result/RobotID" +ss.str()  +".txt";

ofstream out(file.c_str(),ios::trunc); 
                if(out.fail()) 
                { 
                        cerr<<"file error"<<endl; 
                } 
for( int h=0; h <GenomeLength; h++)           //
{  

  for(   int i=0;  i <50; i++)
{
      
 
    out<<   " "<<Cregulation[h][i][0];
         
}
for(int m=0;m<5;m++) 
{
  out<<   "  produce: "<< genes[genome[h][0]]->product[m];
}
   out<<   "  produce concentration: "<< genes[genome[h][0]]->ProductConcen ;
          out<<endl;
 }
     out << " GRN map1 "<<endl;

 for( int h=0; h <GenomeLength; h++)           //
{  

  for(   int i=0;  i <50; i++)
{
      
 
    out<<   " "<<Cregulation[h][i][1];
         
}
          out<<endl;
 }
     out << " GRN map  2 "         <<endl;


	 
 for(int m=0;m<8;m++) 
{
  out<<   " "<< output [m];
}
out << " output map " <<endl;  

   for(int m=0;m <12;m++) 
{
  out<<   " "<< sen [m];
}
out << "  sensor map " <<endl;
out.close();
}
  bool Robot::Checklaststep (double &totalMu)
{
double totalEx=0;
double totalEx2=0;
//cerr<<" checklaststep "<<endl;
   CMucount=0;
  energyCostl=energyCostn; 
   energyCostn =energyCost;
  energyCost=0;

   
  for(int h=0; h  < GenomeLength;h++)
   {
     vote   [h]=0;
	  vote1  [h]=0;
   }
//cerr<<" checklaststep 0 "<<endl;


  for(int h=0; h  < GenomeLength;h++)
  {
  if(genehis[h][2]==1&&genehis[h][1]>0)
  {
   totalEx=totalEx+genehis[h][1]*genes [genome[h][0]]->Pro_num;
  }  
  }
     //cerr<<" checklaststep 1 "<<endl;
  for(int h=0; h  < GenomeLength;h++)
  {
  if(genehis[h][2]==1&&genehis[h][1]>0)
  {
    vote[h]  = 10000*(genehis[h][1]*genes [genome[h][0]]->Pro_num)/totalEx;
	vote1 [h]  = 1;
  } 
  for(int x=0;  x<(genes [genome[h][0]]->Cis_num);x++)
  {
    for(int n=0;  n < GenomeLength;n++)
{
 totalEx2=0;
 int bn;
  if(genehis[n][2]==0&&genehis[n][1]>0)
  {  
 

 
  bn= CheckBind(genes [genome[h][0]]->Regulation[x][0],genes [genome[n][0]]->product[0],genehis[n][1]);          
   totalEx2 =   bn+totalEx2;
  }
  } 
if(totalEx2>0)
{  
    for(int n=0;  n < GenomeLength;n++)
{
   int bn2=CheckBind(genes [genome[h][0]]->Regulation[x][0],genes [genome[n][0]]->product[0],genehis[n][1]);
  if(genehis[n][2]==0&&genehis[n][1]>0&&bn2>0)
  {
  int type=genes [genome[h][0]]->Regulation[x][0]%10;
   if(genes [genome[h][0]]->Regulation[x][0]<500)
    type=type+10;
    vote[n]=  vote[n]+vote[h]*(bn2/totalEx2)*ActiveRate[h][type];
     vote1[n]  = 2;
  }
 }
  }
}	
  }
  
      //cerr<<" checklaststep 2 "<<endl;
   bool ct=true; 
    int layer;
   layer =1;
 while(ct)
 {
      layer++;
	 ct=false;
  for(int h=0; h  < GenomeLength;h++)
  {
  if(genehis[h][2]==0&&genehis[h][1]>0&&vote1[h]==layer)
  {
    for(int x=0;  x<(genes [genome[h][0]]->Cis_num);x++)
  {
   for(int n=0;  n < GenomeLength;n++)
{
 totalEx2=0;
 int bn;
  if(genehis[n][2]==0&&genehis[n][1]>0&&vote1[n]==0)
  {  
   bn= CheckBind(genes [genome[h][0]]->Regulation[x][0],genes [genome[n][0]]->product[0],genehis[n][1]);          
   totalEx2 =   bn+totalEx2;
  }
  } 
  
  
   if(totalEx2>0)
{  
    for(int n=0;  n < GenomeLength;n++)
{
   int bn2=CheckBind(genes [genome[h][0]]->Regulation[x][0],genes [genome[n][0]]->product[0],genehis[n][1]);
  if(genehis[n][2]==0&&genehis[n][1]>0&&bn2>0&&vote1[n]==0)
  {
  int type=genes [genome[h][0]]->Regulation[x][0]%10;
   if(genes [genome[h][0]]->Regulation[x][0]<500)
    type=type+10;
    vote[n]=  vote[n]+vote[h]*(bn2/totalEx2)*ActiveRate[h][type];
     vote1[n]  =layer+1 ;
	      ct=true;
  }
 }
  }
  
  }
  
  }  
  } 
}



          totalMu  = totalMu+Murate;     
if(Mucount>0)
   return true ;
  else
  return false;
   //cerr<<" checklaststep   3 "<<endl;    
  
}



   int Robot::GetExpressv()
{
double  expressv = 0;
int act_n  = 0;
for( int h=0; h <GenomeLength; h++)           //
{  
 if(genes[genome[h][0]]->ProductConcen>=1)
 {
 expressv = expressv + genes[genome[h][0]]->ProductConcen;
  act_n++;
 }
}

  expressv  = expressv/act_n ;
return expressv;
}

void Robot::Eat(   int food  )
{
if(cooprater&&coopraNum>1)
{
double cen  = food*0.1;
energy =energy  +food  + (int)cen;
}
else
 energy =energy  + food;
}

